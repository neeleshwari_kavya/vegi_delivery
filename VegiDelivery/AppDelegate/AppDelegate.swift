//
//  AppDelegate.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 10/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import UIKit
import Foundation
import GooglePlaces
import Fabric
import Crashlytics
import GoogleMaps
import UserNotifications
import AudioToolbox
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
   
  //  var apnsToken = ""
    //Variables
    var window: UIWindow?
    //userdefalt
    var defaltAutoLogin = UserDefaults.standard
    //Variables
    var dictBaseUrl = NSMutableDictionary()
    var dictUserInformations = NSMutableDictionary()
 
   // var intCartCount = 0
    var arrCartList = NSMutableArray()
    var floatTotalPrice = Float()
    var usedRewardsPoints = Float()
   // var  addCartList = UserDefaults.standard
   // var arrMyList = NSMutableArray()
    var arrDataFruits = UserDefaults.standard
    var arrDataVegetables = UserDefaults.standard
    var arrDefaultList = NSMutableArray()
    
    //variables For FCM
    var dictCatchNotifications = NSMutableDictionary()
    var strNotificationToken = ""
    let gcmMessageIDKey = "gcm.message_id"
    var menuClass = "menu"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(PredefinedConstants().googleApiKey)
        GMSPlacesClient.provideAPIKey(PredefinedConstants().googleApiKey)
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        //Messaging.messaging().delegate = self
        
        NotificationCenter.default.addObserver(self, selector:  #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            
            let dddd: [NSObject : AnyObject] = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as! [NSObject : AnyObject]
            DispatchQueue.main.async(execute: {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil, userInfo: dddd)
            })
        }
        
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            let defaults = UserDefaults.standard
            
            print("InstanceID token: \(refreshedToken)")
            defaults.setValue(refreshedToken, forKey: "firebasetoken")
            strNotificationToken = refreshedToken
            print("strNotificationToken: \(strNotificationToken)") //Sachin
        }
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
        
        return true
        
        //header()
        //notification
        
        /*let center  = UNUserNotificationCenter.current()
        center.delegate = self
        Messaging.messaging().remoteMessageDelegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
        }
        application.registerForRemoteNotifications()
        
        // var badgeCount = 0
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.InstanceIDTokenRefresh,object: nil)
        
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            print("strNotificationToken: \(strNotificationToken)")
        }
        return true*/
    }
    
    //fcm Sachin Code ------
    func registerAPNS (_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            //FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            
            let defaults = UserDefaults.standard
            defaults.setValue(refreshedToken, forKey: "firebasetoken")
            
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            print("strNotificationToken: \(strNotificationToken)")// Sachin Notification ID Found *-*-*-*-
            
        } else {
            connectToFcm()
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
    }
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: .sandbox)
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            let defaults = UserDefaults.standard
            defaults.setValue(refreshedToken, forKey: "firebasetoken")
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            
            print("strNotificationToken: \(strNotificationToken)")
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        strNotificationToken = "2bc062e8a3259535e68e0e9ddc60b0792dc190831a5a1ed5f08c31964b13ed31"
        print("Couldn't register: \(error)")
    }
  
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("Notif register: \(userInfo)")
        //        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveRemoteNotification", object: nil, userInfo: userInfo)
        let appReceiptTime  = NSData()
        print("time notification arrival\(appReceiptTime)")
 
    }
    
       //////////////////////============
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil, userInfo:userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("Disconnected from FCM.")
        Messaging.messaging().disconnect()
    }
    func applicationWillEnterForeground(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }

    //////////////////////============
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo// Print message ID.
        print("Message userInfo willPresent:---> \(userInfo)")// completionHandler([])
        
        var dict = NSDictionary()
        dict = userInfo as NSDictionary
        print("userInfodict-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-==\(dict)")
        
        let user_type = string(dict, "user_type")
        print("user_type-\(user_type)-")
        
        let defaults = UserDefaults.standard
        let str: String? = defaults.value(forKey: defaultUser) as? String
        completionHandler([.alert, .badge, .sound])
        //new work
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil, userInfo:notification.request.content.userInfo)
        print("dictCatchNotifications ----------->>>\(dictCatchNotifications)")
        completionHandler([.badge, .sound])

    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let appReceiptTime  = NSDate()
        print("time notification arrival\(appReceiptTime)")
        //  self.playSoundAndVibrateForNotification()
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print(Messaging.messaging().appDidReceiveMessage(userInfo))
        print(userInfo)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil, userInfo: userInfo)
    }
    
    //get notification here from fcm server hp
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        self.playSoundAndVibrateForNotification()
        var dict = NSDictionary()
        dict = userInfo as NSDictionary
        print("userInfodict-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-==\(dict)")
        let user_type = string(dict, "user_type")
        print("user_type-\(user_type)-")
        
    }
    
    func playSoundAndVibrateForNotification() {
        var soundID:SystemSoundID = 0
        let soundFile: String = Bundle.main.path(forResource: "phonering", ofType: "wav")!
        AudioServicesCreateSystemSoundID(NSURL.fileURL(withPath: soundFile) as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("error-\(error)-")
    }
    
    //-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=fcm-=-=-=-=-=-=xxxxxxxxxxxx
    // Fcm Message protocol Functions :
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("New FCM Token received : /(fcmToken)")
        
    }
    @available(iOS 10.0, *)//Sourabh it's very important method ......
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        // What message comes here ?
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil, userInfo: remoteMessage.appData)
        print("dictCatchNotifications ----------->>>\(dictCatchNotifications)")
    }
    
    func applicationReceivedRemoteMessage(remoteMessage: MessagingRemoteMessage){
        print("remoteMessage ========================================= %@", remoteMessage)
    }
    
    
    

}
