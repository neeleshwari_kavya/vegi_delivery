//
//  UserInfo.swift
//  Zooma
//
//  Created by Avinash somani on 26/12/16.
//  Copyright © 2016 Kavyasoftech. All rights reserved.
//

import UIKit

open class UserInfo: NSObject, NSCoding {
    
    open func encode(with aCoder: NSCoder) {
    }
    
    public required init?(coder aDecoder: NSCoder) {
    }
    
    override public init() {
        
    }

    class public func archivePeople(_ people:UserInfo) -> NSData {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: people)
        return archivedObject as NSData
    }
    
    class public func retrievePeople(_ data:NSData) -> UserInfo {
        return (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? UserInfo)!
    }
    
    class public func save (_ ob:UserInfo) {
        let defaults = UserDefaults.standard
        defaults.set(archivePeople(ob), forKey: "LoginInfo")
        defaults.synchronize()
    }
    
    public func save () {
        UserInfo.save(self)
    }
    
    public class func logout () {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "LoginInfo")
        defaults.synchronize()
    }
    
    open class func user1() -> Any? {
        let defaults = UserDefaults.standard
        
        if (defaults.object(forKey: "LoginInfo") != nil) {
            let data = defaults.object(forKey: "LoginInfo") as! NSData
            return retrievePeople(data)
        } else {
            return nil
        }
    }
    
    open func user() ->  Any? {
        return UserInfo.user1()
    }
    
    func setFruitCount(count:Int) {
        let defaults = UserDefaults.standard
        defaults.set(count, forKey: "count_fruits")
        defaults.synchronize()
    }
    
    func setVegCount(count:Int) {
        let defaults = UserDefaults.standard
        defaults.set(count, forKey: "count_veg")
        defaults.synchronize()
    }
    
    
    class func getVegCount(count:Int) -> Int{
        let defaults = UserDefaults.standard
        if let temp = defaults.value(forKey: "count_veg") as? Int {
            
            return temp
        }
        return 0
    }
    
    class func getFruitCount(count:Int) -> Int{
        let defaults = UserDefaults.standard
        if let temp = defaults.value(forKey: "count_fruit") as? Int {
            return temp
        }
        return 0
    }
    
}

class tokenClass: NSObject {
    class func getToken()-> NSMutableDictionary {
        // if let user:NSDictionary = user() {
        if let token:String = string(kAppDelegate.dictUserInformations, "auth_token") {
            // return NSMutableDictionary(dictionary: ["Authorization": "Bearer " + token])
            return NSMutableDictionary(dictionary: ["Authorization":  token])
        }
        // }
        return NSMutableDictionary(dictionary: ["Authorization": "token"])
    }
}










