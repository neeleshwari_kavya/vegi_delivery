//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <UIKit/UIKit.h>
#import "HHPageView.h"//For Tutorial page
#import "DotActivityIndicatorParms.h"//For Loader
#import "DotActivityIndicatorView.h"

#import "RESideMenu.h"
#import "UIViewController+RESideMenu.h"

#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"

//#import "DYQRCodeDecoderViewController.h"//For QR code

//#import "BRScrollLabel.h"
//#import "BRScrollHandle.h"
//#import "BRScrollBarController.h"
//#import "BRCommonMethods.h"//For Custom scroll page
//#import "ALMRZScanViewController.h"
//#import "ALIdentificationView.h"
//#import <Anyline/Anyline.h>//For Passport
///Users/kavyamacmini2/Desktop/Neeleshwari_WorkSpace/Projects/VegiDelivery_work New/VegiDelivery/VegiDelivery/VegiDelivery-Bridging-Header/VegiDelivery-Bridging-Header.h
