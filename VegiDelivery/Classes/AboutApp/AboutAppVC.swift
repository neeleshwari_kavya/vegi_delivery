//
//  AboutAppVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 13/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import MapKit
import CoreLocation
import JVFloatLabeledTextField

class AboutAppVC: UIViewController  , MFMailComposeViewControllerDelegate ,UINavigationControllerDelegate ,CLLocationManagerDelegate {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var lblVersion: UILabel!
    @IBOutlet var tfMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfWebsite: SkyFloatingLabelTextField!
    @IBOutlet var btnMobileNumber: UIButton!
    @IBOutlet var btnEmail: UIButton!
    @IBOutlet var btnWebsite: UIButton!
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var tvAddress: JVFloatLabeledTextView!
    
    //MARK:- Variables
    var strLatitude = String()
    var strLongitude = String()
    var locationManager: CLLocationManager!
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        // self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "ABOUT APP")
        self.menuNavigationButton()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.WS_App_Info()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        let userIconMobile = UIImage(named: "mobile")
        setPaddingWithRightImage(image: userIconMobile!, textField: tfMobileNumber)
        
        let userIconEmail = UIImage(named: "email")
        setPaddingWithRightImage(image: userIconEmail!, textField: tfEmail)
        
        let userIconWebsite = UIImage(named: "website")
        setPaddingWithRightImage(image: userIconWebsite!, textField: tfWebsite)
        
        btnCity.addBottomBorder(color: UIColor.black, height: 1)
        
    }
    
    var address = ""
    func setData(dictResult:NSDictionary) {
        
        tfMobileNumber.text = string(dictResult, "CONTACT_NUMBER")
        tfEmail.text = string(dictResult, "SUPPORT_EMAIL")
        tfWebsite.text =  string(dictResult, "WEB_URL")
        tvAddress.text = string(dictResult, "ADDRESS")
        address = string(dictResult, "ADDRESS")
        let appVersion = string(dictResult, "APP_VERSION")
        lblVersion.text = "Version \(appVersion)"
        tvAddress.font = UIFont.systemFont(ofSize: 15.0)
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnMobileNumber(_ sender: Any) {
        var strMobNumber = self.tfMobileNumber.text!
        if strMobNumber.count != 0{
            
            strMobNumber = strMobNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "")
            
            callNumber(strMobNumber)
        }
    }
    
    @IBAction func btnEmailAction(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            
            self.present(mailComposeViewController, animated: true, completion: nil)
            
        } else {
            Http.alert("Could Not Send Email", "Your device could not send Email. Please check Email configuration and try again.")
        }
    }
    
    @IBAction func btnWebsiteAction(_ sender: Any) {
        guard let url = URL(string: "http://www.vegidelivery.com") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnCityAction(_ sender: Any) {
        getLatLngForZip(zipCode: address)
    }
    
    func getLatLngForZip(zipCode: String) {
        
        let address1 = zipCode.replacingOccurrences(of: "\n", with: " ")
        let address2 = address1.replacingOccurrences(of: "  ", with: " ")
        let address3 = address2.replacingOccurrences(of: " ", with: "%20")
        
        if let url = URL(string: "http://maps.google.com/maps?q=\(address3)") { //
            if (UIApplication.shared.canOpenURL(url)) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    
    /*
     let lat = 22.7196
     let lon = 75.8577
     if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
     {
     UIApplication.shared.openURL(NSURL(string:
     "https://maps.google.com/?q=@\(lat),\(lon)")! as URL)
     }
     }
     */
    
    //MARK:- MFMailComposeViewController delegate
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([tfEmail.text!])
        mailComposerVC.setSubject("Support")
        mailComposerVC.setMessageBody("Hello", isHTML: false)
        
        return mailComposerVC
    }
    
    // MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue :
            print("Cancelled")
            Http.alert("", "Cancelled.")
        case MFMailComposeResult.failed.rawValue :
            print("Failed")
            Http.alert("", "Failed.")
        case MFMailComposeResult.saved.rawValue :
            print("Saved")
            Http.alert("", "Saved.")
        case MFMailComposeResult.sent.rawValue :
            print("Sent")
            Http.alert("", "Sent.")
        default: break
        }
        // Dismiss the mail compose view controller.
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- ------------------WS_App_Info ----------------------
    func WS_App_Info() {
        
        let params = NSMutableDictionary()
        
        Http.instance().json(WebServices().WS_App_Info, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    if let result = json?.object(forKey: "result") as? NSDictionary {
                        print("result>>>\(result)")
                        self.setData(dictResult:result)
                    }
                }
            } else {
                Http.alert("", string(json! as! NSDictionary , "message"))
            }
        }
    }
    
    
}//Class Ends Here ===========================.....Neeleshwari



