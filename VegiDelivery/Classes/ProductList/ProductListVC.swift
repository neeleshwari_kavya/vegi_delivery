//
//  ProductListVC.swift
//  VegiDelivery
//
//  Created by Harish on 25/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

let notificationIdentifier: String = "NotificationIdentifier"

class ProductListVC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource  , UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate , ProductListCellDelegate , UITextFieldDelegate{
    
    @IBOutlet var viewSearch: View!
    @IBOutlet var tfSearchBar: TextField!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var viewProductImage: UIView!
    //Full img
    @IBOutlet var viewImgPopUP: View!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var btnCloseImage: UIButton!
    //view weight
    @IBOutlet var viewWeightMain: UIView!
    @IBOutlet var tblViewWeight: UITableView!
    @IBOutlet var lblSelectWeight: UILabel!
    @IBOutlet var viewWeightConstraintHeight: NSLayoutConstraint!
    @IBOutlet var viewWeightPopUP: View!
    @IBOutlet var btnCloseWtList: UIButton!
    @IBOutlet var lblNoRecord: UILabel!
    
    //MARK:- Variables
    var strTitle = ""
    var category_id = ""
    var arrProductList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    var arrWeightList = NSMutableArray()
    var intWtIndex = 0
    var intIndexVeg = -1
    var arrJson = NSMutableArray()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        //setNavVegiDeliveryGreen(className: strTitle)
        setNavShopping(className:strTitle)
        self.menuNavigationButton()
        
        lblNoRecord.isHidden = true
        //==========================Set data for app Delegates======================
        //////////////////
        page = 1
        self.WS_ProductList()
        
        if category_id == "1" {
             PredefinedConstants.appDelegate.arrDefaultList = PredefinedConstants().GetVeg____SynData()
        }else{
            PredefinedConstants.appDelegate.arrDefaultList = PredefinedConstants().GetFruits____SynData()
        }

        print("When we coming./.......................\(PredefinedConstants.appDelegate.arrDefaultList)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
    }
  
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    let lblTotalCount = UILabel()
    func setNavShopping(className:String)  {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "appTheme")
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 60, height: 40))
        self.navigationItem.titleView = titleViewNav
        
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: 50, height: 40))
        lbl1.text = className
        lbl1.font = UIFont.boldSystemFont(ofSize: 18.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        lbl1.sizeToFit()
        lbl1.center.y = titleViewNav.center.y
        if lbl1.frame.size.width > self.view.frame.size.width - 170 {
            lbl1.frame.size.width = self.view.frame.size.width - 170
        }
        
        titleViewNav.addSubview(lbl1)
        //////////////
        
        let btnNavCartLarge = UIButton(type: .custom)
        btnNavCartLarge.frame = CGRect(x: titleViewNav.frame.size.width - 50, y: 0, width: 50, height: 50)
        btnNavCartLarge.addTarget(self, action: #selector(actionCart), for: .touchUpInside)
        titleViewNav.addSubview(btnNavCartLarge)
        btnNavCartLarge.backgroundColor = UIColor.clear
        
        ///=========
        
        let imgN = imageWithImage(image: UIImage(named: "cart_menu")!, scaledToSize: CGSize(width: 25.0, height: 25.0))
        
        let btnNavCart = UIButton(type: .custom)
        btnNavCart.setImage(imgN, for: .normal)
        btnNavCart.frame = CGRect(x: titleViewNav.frame.size.width - 50, y: 5, width: 30, height: 30)
        btnNavCart.contentMode = .scaleAspectFit
        btnNavCart.addTarget(self, action: #selector(actionCart), for: .touchUpInside)
        titleViewNav.addSubview(btnNavCart)
        btnNavCart.backgroundColor = UIColor.clear
        
        lblTotalCount.frame =  CGRect.init(x: btnNavCart.frame.origin.x + 25 , y: 0, width: 20 , height: 20)
        
        let cartCount = PredefinedConstants().GetVeg____SynData().count + PredefinedConstants().GetFruits____SynData().count
        lblTotalCount.text = "\(cartCount)"//"15"
        
        if cartCount == 0 {
            lblTotalCount.isHidden = true
        }else{
            lblTotalCount.isHidden = false
        }
        
        lblTotalCount.font = UIFont.systemFont(ofSize: 10.0)
        lblTotalCount.textColor = UIColor.white
        lblTotalCount.numberOfLines = 1
        lblTotalCount.backgroundColor = UIColor(named: "redColor")
        
        lblTotalCount.textAlignment = .center
        titleViewNav.addSubview(lblTotalCount)
        lblTotalCount.layer.cornerRadius = lblTotalCount.frame.size.width/2
        lblTotalCount.clipsToBounds = true
    }
    
    @objc func actionCart()  {
        print("actionCartCount")
        
        let newVC = self.storyboard?.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
        newVC.lastClass = "product"
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        tblViewWeight.delegate = self
        tblViewWeight.dataSource = self
        tfSearchBar.delegate = self
        tfSearchBar.placeholder = "Search product Here..."
        tfSearchBar.padding(5)
        tfSearchBar.border1(UIColor(named: "appTheme"), 5, 2)
        
        tblViewWeight.isScrollEnabled = false
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let itemSpacing: CGFloat = 1
        let itemsInOneLine: CGFloat = 2
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: 196)
        flow.minimumInteritemSpacing = 1
        flow.minimumLineSpacing = 1
        
        collectionView.backgroundColor = UIColor.white
    }
    
    //MARK:- Button Actions
    @IBAction func btnCloseImageAction(_ sender: Any) {
        removeSubViewWithAnimation(viewMain: viewProductImage, viewPopUP: viewImgPopUP)
    }
    
    @IBAction func btnCloseWtList(_ sender: Any) {
        self.selectWeightIndexActions()
    }
    func selectWeightIndexActions()  {
        removeSubViewWithAnimation(viewMain: viewWeightMain, viewPopUP: viewWeightPopUP)
        
        let dictVeg : NSDictionary = arrProductList.object(at: intIndexVeg) as! NSDictionary
        dictVeg.setValue(intWtIndex, forKey: "cart_index")
        arrProductList.replaceObject(at: intIndexVeg, with: dictVeg)
        
        collectionView.reloadData()
    }

    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfSearchBar {
            // self.WS_ProductList()
            tfSearchBar.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfSearchBar {
            
            self.WS_ProductList()
            return (length > 50) ? false : true
        }
        return true
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowView(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewMain)
        //self.view.addSubview(viewMain)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
    }
    var popUpSelected = "table"
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewProductImage, viewPopUP: viewImgPopUP)
        removeSubViewWithAnimation(viewMain: viewWeightMain, viewPopUP: viewWeightPopUP)
        
        if popUpSelected == "table"{
            
            let dictVeg : NSDictionary = arrProductList.object(at: intIndexVeg) as! NSDictionary
            
            dictVeg.setValue(intWtIndex, forKey: "cart_index")
            arrProductList.replaceObject(at: intIndexVeg, with: dictVeg)
            collectionView.reloadData()
        }
        
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewProductImage.bounds.contains(touch.location(in: viewImgPopUP)) {
            return false
        }
        if viewWeightMain.bounds.contains(touch.location(in: viewWeightPopUP)) {
            return false
        }
        return true
    }
    
    //MARK:- Table View delegates and datasource  ==================
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrWeightList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblViewWeight.dequeueReusableCell(withIdentifier: "SelectWeightCell") as! SelectWeightCell
        
        let dictData = arrWeightList.object(at: indexPath.row) as! NSDictionary
        
        cell.lblWeight.text = string(dictData, "option_name")
        
        if intWtIndex == indexPath.row{
            cell.imgWeight.image = UIImage(named: "check_circle")
            
        }else{
            cell.imgWeight.image = UIImage(named: "uncheck_circle")
        }
        cell.contentView.backgroundColor = UIColor.white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        intWtIndex = indexPath.row
        tblViewWeight.reloadData()
        self.selectWeightIndexActions()
    }
    
    var boolHideMulti = true
    var intCollectionIndex = -1
    
    //MARK:- Colletion View delegates and datasource ==================
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrProductList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath) as! ProductListCell
        
        let dictVeg : NSDictionary = arrProductList.object(at: indexPath.row) as! NSDictionary
        
        print("dictVeg>>\(dictVeg)")
        
        cell.lblName.text = string(dictVeg, "title")
        cell.delegateProductListCell = self
        cell.intIndex = indexPath.row
        
        cell.viewMultiple.isHidden = true
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dictVeg, "filename")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            cell.imgProduct.image = UIImage(named: "vegi_default")
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            cell.imgProduct.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
        
        //==============================================================
        
        let cart_index = number(dictVeg, "cart_index")
        print("cart_index--->>>>\(cart_index)")
        
        if let arr = dictVeg.object(forKey: "product_option") as?  NSArray {
            self.arrWeightList  = arr.mutableCopy() as! NSMutableArray
        }
        
      //  let dictProduct_option = (arrWeightList.object(at: Int(cart_index)) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        let dictProduct_option = arrWeightList.object(at: Int(cart_index)) as! NSDictionary
        
        let cart_quantity = Int(number(dictProduct_option, "cart_quantity"))
        
        if cart_index == 0 {
            cell.lblPrice.text = "₹\(string(dictVeg, "price"))"
        }else{
            cell.lblPrice.text = "₹\(string(dictProduct_option, "price"))"
        }
        cell.lblWeight.text = "\(string(dictProduct_option, "option_name"))"
        cell.lblTotal.text = "\(cart_quantity)"
        
        if cart_quantity > 0 {
            cell.btnAdd.isHidden = true
            cell.viewMultiple.isHidden = false
            
        }else{
            cell.btnAdd.isHidden = false
            cell.viewMultiple.isHidden = true
        }
        cell.activityIndicator.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
          cell.activityIndicator.isHidden = true
        })
        
        ////////////================================
        if indexPath.row == (arrProductList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrProductList.count % 50 == 0 {
                   // print("indexPath.row:\(indexPath.row)")
                    page += 1
                    self.WS_ProductList()
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    //MARK:- Colletion view Delegate Actions
    func btnPlus(intIndex: Int){
        addToCartUpdates(strAction: "plus", intIndex: intIndex)
    }
    
    func btnMinus(intIndex: Int){
        addToCartUpdates(strAction: "minus", intIndex: intIndex)
    }
    
    func btnAdd(intIndex: Int){
        
        addToCartUpdates(strAction: "add", intIndex: intIndex)
    }
     //MARK:- Delegate Actions Fuction for ADD,PLUS AND MINUS
    func addToCartUpdates(strAction:String,intIndex: Int) {
        intIndexVeg = intIndex
        let dictData = (arrProductList.object(at: intIndexVeg) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        var cart_quantity = 0
        
        let cart_index = number(dictData, "cart_index")
        print("cart_index>>\(cart_index)")
        var dictProduct_option = NSMutableDictionary()
        
        if let arr = dictData.object(forKey: "product_option") as?  NSArray {
            let arrMutable  = arr.mutableCopy() as! NSMutableArray
            
            dictProduct_option = (arrMutable.object(at: Int(cart_index)) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
            cart_quantity = Int(number(dictProduct_option, "cart_quantity"))
            
            
            if strAction == "add"{
                cart_quantity = 1
                
            }else if strAction == "plus"{
                
                if cart_quantity == 99{
                    Http.alert("", "Limit Exceeded")
                }else{
                    cart_quantity += 1
                }
            }else{
                cart_quantity -= 1
            }
            
            dictProduct_option.setObject(cart_quantity, forKey: "cart_quantity" as NSCopying)
            arrMutable.replaceObject(at: Int(cart_index), with: dictProduct_option)
            dictData.setValue(arrMutable, forKey: "product_option")
            //==========================================
            //Sourabh add this one....
            //==========================================
            self.setNEWUpdatedArray(dictData:dictData)
            //==========================================
            ///===========================================
            arrProductList.replaceObject(at: intIndexVeg, with: dictData)
            collectionView.reloadData()
            
            let id = string(dictData, "id")
            var price = ""
            
            if cart_index == 0 {
                price = string(dictData, "price")
            }else{
                price = string(dictProduct_option, "price")
            }
            
            let weight = string(dictProduct_option, "option_name")
            
            let dictOption = NSMutableDictionary()
            dictOption.setValue("\(weight)", forKey: "weight")
            arrJson.add(["option": dictOption,"product_id": id ,"quantity": cart_quantity ,"price": price])
            
            //add to cart
            ws_ADD_TO_CART()
        }
        
    }
   // MARK:- Function to set and get Default Poduct Lists
      ///////////////////////////////////////////////////======

    //Sourabh add this one....
    ///===========================================
    
    func setNEWUpdatedArray(dictData:NSMutableDictionary) {
   
        if PredefinedConstants.appDelegate.arrDefaultList.count > 0 {
            
            var notVaild : Bool? = false
            
            for i in 0..<PredefinedConstants.appDelegate.arrDefaultList.count{
                
                var dd = NSMutableDictionary()
                dd = PredefinedConstants.appDelegate.arrDefaultList.object(at: i) as! NSMutableDictionary
                
                if string(dd, "id") == string(dictData, "id"){
                    PredefinedConstants.appDelegate.arrDefaultList.replaceObject(at: i, with: dictData)
                    
                    notVaild = true
                    if category_id == "1" {
                        PredefinedConstants().storeVeg____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
                        
                    }else{
                        PredefinedConstants().storeFruits____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
                        
                    }
                    break
                    
                }
                
            }
            
            //==========================================
       
            if notVaild == false{
                PredefinedConstants.appDelegate.arrDefaultList.add(dictData)
                
                if category_id == "1" {
                    PredefinedConstants().storeVeg____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
                }else{
                    PredefinedConstants().storeFruits____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
                }
            }
            
        }else{
            PredefinedConstants.appDelegate.arrDefaultList.add(dictData)
            
            if category_id == "1" {
                PredefinedConstants().storeVeg____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
            }else{
                PredefinedConstants().storeFruits____SynData(str: PredefinedConstants.appDelegate.arrDefaultList)
            }
        }
        
        let cartCount = PredefinedConstants().GetVeg____SynData().count + PredefinedConstants().GetFruits____SynData().count
        lblTotalCount.text = "\(cartCount)"//"15"
        
        if cartCount == 0 {
            lblTotalCount.isHidden = true
        }else{
            lblTotalCount.isHidden = false
        }
        print("PredefinedConstants().GetVeg____SynData()>>>\(PredefinedConstants().GetVeg____SynData())")
        
        
    }
    //==========================================
    
    ///=========
    ///////////////////////////////////////////////////======
    func btnShowWeightList(intIndex: Int){
        
        let dictVeg : NSDictionary = arrProductList.object(at: intIndex) as! NSDictionary
        
        let cart_index = number(dictVeg, "cart_index")
        print("cart_index--->>>>\(cart_index)")
        // intIndex = cart_index
        intWtIndex = Int(cart_index)
      //  tblViewWeight.reloadData()
        
        intIndexVeg = intIndex
        let dictData = arrProductList.object(at: intIndex) as! NSDictionary
        
        print("dictData--------->>>\(dictData)")
        
        if let arr = dictData.object(forKey: "product_option") as?  NSArray {
            
            print("arr--------->>>\(arr)")
            
            self.arrWeightList  = arr.mutableCopy() as! NSMutableArray
           
            let tblHt = Double(arrWeightList.count)
            let cellHt = 40.0
            let viewHt = tblHt*cellHt + 65.0
            
            viewWeightConstraintHeight.constant = CGFloat(viewHt)
            tblViewWeight.backgroundColor = UIColor.white
            self.tblViewWeight.reloadData()
        }
        
        addPopupShowView(viewMain: viewWeightMain, viewPopUP: viewWeightPopUP)
        popUpSelected = "table"
        
    }
    
    func btnShowFullImage(intIndex: Int){
        
        intIndexVeg = intIndex
        
        let dictData = arrProductList.object(at: intIndexVeg) as! NSDictionary
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dictData, "filename")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            imgProduct.image = UIImage(named: "vegi_default")
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            imgProduct.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
        
        addPopupShowView(viewMain: viewProductImage, viewPopUP: viewImgPopUP)
        
    }
    
    //MARK:- ------------------ cart List ----------------------//
    func WS_ProductList() {
        
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        let user_id = string(dictInfo, "id")
        
        let params = NSMutableDictionary()
        params["category_id"] = category_id
        params["search_value"] = tfSearchBar.text
        params["user_id"] = user_id
        params["page"] = page
        Http.instance().json(WebServices().WS_CATEGORY_PRODUCT, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrProductList = NSMutableArray()
                    if let result = json?.object(forKey: "result") as?  NSDictionary {
                        if let arr = result.object(forKey: "category_product_list") as?  NSArray {
                            self.arrProductList  = arr.mutableCopy() as! NSMutableArray
                            self.addProductQuantity()
                          
                            for i in 0..<self.arrProductList.count{
                                
                                var dd = NSMutableDictionary()
                                dd = self.arrProductList.object(at: i) as! NSMutableDictionary
                         
                                for j in 0..<PredefinedConstants.appDelegate.arrDefaultList.count {
                                    
                                    var dict = NSMutableDictionary()
                                    dict = PredefinedConstants.appDelegate.arrDefaultList.object(at: j) as! NSMutableDictionary
                                    
                                    if string(dd, "id") == string(dict, "id"){
                                     self.arrProductList.replaceObject(at: i, with: dict)
                                        break
                                    }
                                }
                            }
                            
                            if self.arrProductList.count == 0 {
                                self.lblNoRecord.isHidden  = false
                            }else{
                                self.lblNoRecord.isHidden  = true
                            }
                            self.collectionView.reloadData()
                        }
                    }
                } else {
                    // Http.alert("", string(json! , "message"))
                    self.arrProductList = NSMutableArray()
                    self.lblNoRecord.isHidden  = false
                    self.lblNoRecord.text = string(json! , "message")
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    func addProductQuantity() {
        
        if  arrProductList.count != 0{
            for i in 0..<arrProductList.count {
                let dictVeg = (arrProductList.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                if let arr = dictVeg.object(forKey: "product_option") as?  NSArray {
                    let arrMutable  = arr.mutableCopy() as! NSMutableArray
                    for j in 0..<arrMutable.count {
                        let dictProduct_option = (arrMutable.object(at: j) as! NSDictionary)
                        let dictP : NSMutableDictionary = dictProduct_option.mutableCopy() as! NSMutableDictionary
                        dictP.removeObject(forKey: "option_hindi_name")
                        dictP.removeObject(forKey: "option_group_hindi_name")
                        dictP.setObject(0, forKey: "cart_quantity" as NSCopying)
                        arrMutable.replaceObject(at: j, with: dictP)
                    }
                    dictVeg.setValue(arrMutable, forKey: "product_option")
                }
                dictVeg.removeObject(forKey: "hindi_name")
                dictVeg.setObject(0, forKey: "cart_index" as NSCopying)
                arrProductList.replaceObject(at: i, with: dictVeg)
            }
            self.collectionView.reloadData()
        }
    }
    
    //MARK:- Add to cart
    
    func ws_ADD_TO_CART() {
        
        let params = NSMutableDictionary()
        params["cart_json"] = arrJson.convertToString()//convertArrayToJson(arrJson)
        Http.instance().json(WebServices().WS_Product_Add_To_Cart, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                   // Http.alert("", string(json! , "message"))
                    //  _ = self.navigationController?.popViewController(animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//Class Ends Here ===========================.....Neeleshwari

//MARK:- ------------------ ProductListCell ----------------------//

public protocol ProductListCellDelegate{
    func btnPlus(intIndex: Int)
    func btnMinus(intIndex: Int)
    func btnAdd(intIndex: Int)
    func btnShowWeightList(intIndex: Int)
    func btnShowFullImage(intIndex: Int)
}

class ProductListCell: UICollectionViewCell {
    
    @IBOutlet var viewBG: View!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var viewMultiple: UIView!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnWeightOptions: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var btnShowFullImage: UIButton!
    
    var delegateProductListCell:ProductListCellDelegate?
    var intIndex = Int()
    
    //MARK:- Button Actions
    
    @IBAction func btnAddAction(_ sender: Any) {
        delegateProductListCell?.btnAdd(intIndex: intIndex)
        
    }
    @IBAction func btnMinusAction(_ sender: Any) {
        delegateProductListCell?.btnMinus(intIndex: intIndex)
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        delegateProductListCell?.btnPlus(intIndex: intIndex)
    }
    
    @IBAction func btnWeightOptions(_ sender: Any) {
        delegateProductListCell?.btnShowWeightList(intIndex: intIndex)
    }
    
    @IBAction func btnShowFullImage(_ sender: Any) {
        delegateProductListCell?.btnShowFullImage(intIndex: intIndex)
    }
}

//MARK:- ------------------ SelectWeightCell ----------------------//

class SelectWeightCell :UITableViewCell{
    
    @IBOutlet var imgWeight: UIImageView!
    @IBOutlet var lblWeight: UILabel!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
}

//////////////////////=================


public extension NSArray {
    public func convertToString () -> String {
        do {
            let jsonData: NSData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            
            let str = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            
            return str.replacingOccurrences(of: "\n", with: "")
        } catch {
            
        }
        
        return "[]"
    }
}

///////////////===============
func convertArrayToJson(_ arr: NSMutableArray) -> String {
    
    return arr.convertToString()
    /*if let theJSONData = try? JSONSerialization.data(
     withJSONObject: arr,
     options: []) {
     let theJSONText = String(data: theJSONData, encoding: .ascii)
     return theJSONText!
     }
     
     return ""*/
}
///////////////=====================================================



