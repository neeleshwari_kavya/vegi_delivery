//
//  CheckOutVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 11/05/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class CheckOutVC: UIViewController , UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate , UITextViewDelegate , CheckOutAddressCellDelegate , CheckoutRewardCellDelegate{
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewConfirm: NSLayoutConstraint!
    @IBOutlet var btnConfirm: UIButton!
    
    //MARK:- Variables
   // var arrCartList = NSMutableArray()
    var arrAddressList = NSMutableArray()
    var strPaymentMode = ""
    var intAddressSelected = -1
    var intPaymentModeSelected = -1
    var boolWsPage = Bool()
    var page = Int()
    var floatTotalAmount = Float()
    var arrCartList = NSMutableArray()
    var commentText = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        floatTotalAmount = PredefinedConstants.appDelegate.floatTotalPrice
        print("arrCartList>>\(arrCartList)")
        titleRewardButton = "Apply Rewards"
      //   self.registerForKeyboardNotifications()
       // self.tblView.autoresizingMask = .flexibleHeight
        //UIViewAutoresizingFlexibleHeight;
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        let user_reward_points =  string(dictInfo, "user_reward_points").count
        
        var rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
        
        intRewardValue = Int(rewardPoints)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: Keyboard Notifications
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "CHECKOUT")
        self.menuNavigationButton()
        // self.registerForKeyboardNotifications()
        
        page = 1
        self.WS_AddressList()
        //  self.setTaxesDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.deregisterFromKeyboardNotifications()
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        btnConfirm.border(UIColor(named: "redColor"), 20, 1.0)
    }

    //MARK:- BUtton Actions
    
    @IBAction func btnConfirmOrder(_ sender: Any) {
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            //  self.ws_SignIn()
            self.setCreateOrderJsonArrayData()
        }
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
       // scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
       // scrlView.contentInset = UIEdgeInsets.zero
    }
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if intAddressSelected == -1 {
            return AlertMSG.selectAddress
            
        }
        /*
         else if strPaymentMode.count == 0  {
         return AlertMSG.selectPaymentMethod
         }*/
        
        return nil
    }
    /*
     func checkPasswordValidation() -> String? {
     
     if tvReason.text?.count == 0 {
     return AlertMSG.cancelOrderReason
     }
     return nil
     }*/
    
    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return arrCartList.count//arrOrders.count + 1
        }else if section == 1{
            return 1
        }else if section == 2{
            return arrAddressList.count
        }
        else if section == 3{
            return 1
        }else if section == 4{
            return 1
        }else if section == 5{
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tblView.dequeueReusableCell(withIdentifier: "OrdersDetailCell") as! OrdersDetailCell
        if arrCartList.count != 0{
            let dict = arrCartList.object(at: indexPath.row) as! NSDictionary
            
            print("dict>>\(dict)")
            
            cell.lblName.text = string(dict, "title")
            cell.lblPrice.text = "₹\(string(dict, "price"))"//"Price: ₹4545.20"
            cell.lblQuantity.text = string(dict, "quantity")
            
            let strOption = string(dict, "option")
            let dictOption = convertToDictionary(text: strOption)! as NSDictionary
            cell.lblWeight.text = string(dictOption , "weight")
            let amt = Float(number(dict, "price")) * Float(string(dict, "quantity"))!
            
            cell.lblTotalPrice.text = "₹\(amt)"///
            
            //    ===============================
            cell.activityIndicator.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                cell.activityIndicator.isHidden = true
            })
            //    ===============================
            //product image
            let UserImage_URLString = WebServices().WEB_URL
            let strUserImg = string(dict, "image")
            let imageUrl = UserImage_URLString + strUserImg
            
            if strUserImg.isEmpty {
                cell.imgOrder.image = UIImage(named: "vegi_default")
            } else {
                let url =  NSURL(string: imageUrl)
                // cell.imgProduct.af_setImage(withURL: url as URL!)
                cell.imgOrder.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
            }
            
         
        }
            return cell
            
        }else if indexPath.section == 1{
            let cell = tblView.dequeueReusableCell(withIdentifier: "TaxDetailCell") as! TaxDetailCell
           
            //++++++++++++++++++++++++++++++++++++
            var rewardPoints = Float()
            
            let SHIPPING_CHARGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "SHIPPING_CHARGE"))"
            let GST_PERCENTAGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "GST_PERCENTAGE"))"
            /*
            let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
            let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
            let user_reward_points =  string(dictInfo, "user_reward_points").count
            
            rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
            */
            let gstPrice :Float = Float(GST_PERCENTAGE)! * floatTotalAmount * 0.01
            
            cell.lblTotalAmount.text = String(format: "Total: ₹ %.2f", floatTotalAmount)//"Total: ₹" + "\(floatTotalAmount)"//string(dictOrderDetails, "total")
            cell.lblGST.text = "Gst(+\(GST_PERCENTAGE)%): ₹ " + String(format: "%.2f", gstPrice) //"Gst(+\(GST_PERCENTAGE)%): ₹" + "\(gstPrice)"
            cell.lblShippingCharges.text =  String(format: "Shipping Charge(+): ₹ %.2f", Float(SHIPPING_CHARGE)!)//"Shipping Charge(+): ₹" + "\(SHIPPING_CHARGE)"
            cell.lblRewards.text =  String(format: "Reward(-): ₹ %.2f", Float(intRewardValue))//"Reward(-): ₹" + "\(rewardPoints)"
            
            let finalTotal = floatTotalAmount +  Float(SHIPPING_CHARGE)! + gstPrice - Float(intRewardValue)
            print("finalTotal---->>\(finalTotal)")
            
            cell.lblGrandTotal.text = String(format: "Grand Total: ₹ %.2f", finalTotal)//"Grand Total: ₹" + "\(finalTotal)"
            //++++++++++++++++++++++++++++++++++++
            
            
            return cell
        }else if indexPath.section == 2{
            
            let cell = tblView.dequeueReusableCell(withIdentifier: "CheckOutAddressCell") as! CheckOutAddressCell
          
            if arrAddressList.count != 0{
             
                cell.btnChecked.layer.cornerRadius =   cell.btnChecked.frame.size.height/2
                cell.btnChecked.clipsToBounds = true
                cell.btnChecked.layer.borderColor = UIColor.red.cgColor
                cell.btnChecked.layer.borderWidth = 0
                cell.selectionStyle = .none
                
                let dictData = arrAddressList.object(at: indexPath.row) as! NSDictionary
                
                cell.lblAddress.text = string(dictData, "address")
                
                if intAddressSelected == indexPath.row{
                    cell.imgCheckCircle.image = UIImage(named: "check_circle")
                    
                    //cell.btnChecked.setImage(UIImage(named: "check_circle"), for: .normal)
                }else{
                    //  cell.btnChecked.setImage(UIImage(named: "uncheck_circle"), for: .normal)
                    cell.imgCheckCircle.image = UIImage(named: "uncheck_circle")
                }
                if indexPath.row == (arrAddressList.count - 1) {
                    if !boolWsPage {
                        boolWsPage = true
                        if arrAddressList.count % 50 == 0 {
                            page += 1
                            self.WS_AddressList()
                        }
                    }
                }
                cell.delegateCheckOutAddressCell = self
                cell.intIndex = indexPath.row
            }

            return cell
        }else if indexPath.section == 3{
            let cell = tblView.dequeueReusableCell(withIdentifier: "CheckOutAddressCell") as! CheckOutAddressCell
            
            cell.btnChecked.layer.cornerRadius =   cell.btnChecked.frame.size.height/2
            cell.btnChecked.clipsToBounds = true
            cell.btnChecked.layer.borderColor = UIColor.red.cgColor
            cell.btnChecked.layer.borderWidth = 0
            
        //    cell.selectionStyle = .none
            
            intPaymentModeSelected = 0
            
            if intPaymentModeSelected == indexPath.row{
                cell.imgCheckCircle.image = UIImage(named: "check_circle")
                //cell.btnChecked.setImage(UIImage(named: "check_circle"), for: .normal)
            }else{
                //  cell.btnChecked.setImage(UIImage(named: "uncheck_circle"), for: .normal)
                cell.imgCheckCircle.image = UIImage(named: "uncheck_circle")
            }
            cell.lblAddress.text = "COD"
            
            cell.delegateCheckOutAddressCell = self
            cell.intIndex = indexPath.row
            
            return cell
        }else if indexPath.section == 4{
            let cell = tblView.dequeueReusableCell(withIdentifier: "CheckoutRewardCell") as! CheckoutRewardCell
            
            let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
            let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
            let user_reward_points =  string(dictInfo, "user_reward_points").count
            var rewardPoints = Float()
            rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
            
          //    cell.lblRewards.text =  String(format: "Reward(-): ₹ %.2f", rewardPoints)//"Reward(-): ₹" + "\(rewardPoints)"
            cell.lblYourRewards.text = String(format: "Your Rewards: ₹ %.2f", rewardPoints)//"Reward(-): ₹" + "
            cell.lblRewardValue.text = "\(intRewardValue)"
          
            ///rewardss
   
            print("titleRewardButton>>\(titleRewardButton)")
            
            if titleRewardButton == "Apply Rewards"{
                cell.viewBGRewardsConstraintHeight.constant = 0
                cell.viewBGRewards.isHidden = true
            }else if titleRewardButton == "Set Rewards"{
                cell.viewBGRewardsConstraintHeight.constant = 48
              cell.viewBGRewards.isHidden = false
            }else if titleRewardButton == "Change Rewards"{
                cell.viewBGRewardsConstraintHeight.constant = 0
                  cell.viewBGRewards.isHidden = true
            }else{
                cell.viewBGRewardsConstraintHeight.constant = 0
                  cell.viewBGRewards.isHidden = true
            }
            
            cell.btnRewards.setTitle(titleRewardButton, for: .normal)
            
            cell.delegateCheckoutRewardCell = self
            cell.intIndex = indexPath.row
            
            
            // cell.lblInstruction.
             return cell
        }else if indexPath.section == 5{
            let cell = tblView.dequeueReusableCell(withIdentifier: "CheckoutInstructionCell") as! CheckoutInstructionCell
            
           // cell.lblPlaceholder.isHidden = true
            cell.tvInstruction.delegate = self
            // cell.lblInstruction.
            return cell
        }else{
            let cell = tblView.dequeueReusableCell(withIdentifier: "shippingDetailCell") as! shippingDetailCell
            
            return cell
        }
    }
    var intRewardValue = 0
   // var intRewardText =  0
    var titleRewardButton = ""
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2{
            intAddressSelected = indexPath.row
            tblView.reloadData()
        }else if indexPath.section == 3 {
            intPaymentModeSelected = indexPath.row
            tblView.reloadData()
        }else{
            
        }
    }
    
    func btnCheckCircle(intIndex: Int){
        intAddressSelected = intIndex
        tblView.reloadData()
    }

    //Mark:-tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var viewHeader = UIView()
        
        var ht = CGFloat()
        var lblTitleY = CGFloat()
        
        if section == 2{
            ht = 40
            lblTitleY = 0
        }else if section == 3{
            ht = 50
            lblTitleY = 10
        }else{
            ht = 50
            lblTitleY = 10
        }
       
        viewHeader = UIView(frame: CGRect(x: 7, y: 0, width: self.tblView.frame.size.width - 14, height: ht))
        
        let  viewBG = UIView(frame: CGRect(x: 7, y: lblTitleY, width: self.tblView.frame.size.width - 14, height: 40))
        
        let lblSectionTitle = UILabel(frame: CGRect(x: 5, y: 0, width: self.tblView.frame.size.width - 70, height: 40))
        
        lblSectionTitle.font =  UIFont.systemFont(ofSize: 15, weight: .semibold)
        lblSectionTitle.textColor = UIColor.black
        lblSectionTitle.numberOfLines = 1
        
        viewBG.addBottomBorder(color: UIColor.black, height: 1)
        
        let btnAddNew = UIButton(frame: CGRect(x: self.tblView.frame.size.width - 55, y: 0, width: 40, height: 40))
        btnAddNew.setImage(UIImage(named: "add_green"), for: .normal)
        btnAddNew.setTitle("", for: .normal)
        btnAddNew.addTarget(self, action: #selector(actionAddNew), for: .touchUpInside)
        
        if section == 0 {
            lblSectionTitle.text = ""
            
        }else if section == 1 {
            lblSectionTitle.text = ""
            
        }else if section == 2 {
            lblSectionTitle.text = "Shipping Address"
            viewBG.addSubview(btnAddNew)
            
        }else if section == 3 {
            lblSectionTitle.text = "Payment Methods"
            
        }else if section == 4 {
            lblSectionTitle.text = ""
            
        }else if section == 5 {
            lblSectionTitle.text = "Special Instruction"
            
        }else {
            
        }
 
        viewBG.addSubview(lblSectionTitle)
        viewHeader.addSubview(viewBG)
        viewBG.backgroundColor = UIColor.white
        lblSectionTitle.backgroundColor = UIColor.clear
        btnAddNew.backgroundColor = UIColor.clear
        viewHeader.backgroundColor = UIColor.clear
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            
            return 0//arrOrders.count + 1
        }else if section == 1{
            return 0
        }else if section == 2{
            
            return 40
        }
        else if section == 3{
            return 40
        }else if section == 4{
            return 0
        }else if section == 5{
            return 40
        }else {
            return 0
        }
    }
    
    var dictOrderDetails = NSMutableDictionary()
    var dictOrderAddress = NSMutableDictionary()
    
    @objc func actionAddNew() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //
    //MARK:- ------------------ WS_AddressList ----------------------//
    
    func WS_AddressList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        Http.instance().json(WebServices().WS_CustomerAddressList, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrAddressList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                       // print("arr>>>\(arr)")
                        
                        if self.boolWsPage {
                            self.arrAddressList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrAddressList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        self.tblView.reloadData()
                    }
                    
                } else {
                    //  Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
   // var used_reward_points = <#value#>
    
    //MARK:- ------------------ WS_ORDER_CREATE ----------------------//
    //var dictJson = NSMutableArray()
    var dictJson =  NSMutableDictionary()
    let arrItem_details = NSMutableArray()
    
    func setCreateOrderJsonArrayData() {
        
        // dictJson
        //  dictJson.add(["option": dictOption,"product_id": id ,"quantity": cart_quantity ,"price": price])
        //var total  = Float()
        
        for i in 0..<arrCartList.count {
            let dictItem = arrCartList.object(at: i) as! NSDictionary
            let product_id = string(dictItem, "product_id")
            let name = string(dictItem, "title")
            let offer_id = string(dictItem, "offer_id")
            let offer = string(dictItem,"offer")
            let offer_type = string(dictItem,"offer_type")
            let quantity = string(dictItem,"quantity")
            let price = string(dictItem, "price")
            let offer_price = string(dictItem,"offer_price")
            let subtotal = string(dictItem,"subtotal")
            let reward = string(dictItem,"reward")
            let strOption = string(dictItem, "option")
            let dictOption = convertToDictionary(text: strOption)! as NSDictionary
            let weight = string(dictOption, "weight")
            
            floatTotalAmount =  PredefinedConstants.appDelegate.floatTotalPrice
            
            let newDictOption = NSMutableDictionary()
            newDictOption.setValue(weight, forKey: "weight")
            
            arrItem_details.add(["product_id": product_id,"name": name ,"offer_id": offer_id ,"offer": offer ,"offer_type": offer_type ,"quantity": quantity ,"price": price ,"offer_price": offer_price ,"subtotal": subtotal ,"reward": reward ,"option": newDictOption ])
        }
      // dictJson.setValue(arrItem_details.convertToString(), forKey: "item_details") //neeleshwari
       print("arrItemJson>>\(arrItem_details.convertToString())")
        
       dictJson.setValue(arrItem_details, forKey: "item_details")
        //arrShipping_details
        
        let dictAddress = arrAddressList.object(at:intAddressSelected )  as! NSDictionary
        
        let shipping_name = string(dictAddress, "name")
        let shipping_mobile = string(dictAddress, "mobile_number")
        let shipping_city = string(dictAddress, "city_name")
        let shipping_address = string(dictAddress, "address")
        let shipping_state = string(dictAddress, "state_name")
        let shipping_country = string(dictAddress, "country_name")
        let shipping_pincode = string(dictAddress, "pincode")
        let shipping_latitude = string(dictAddress, "latitude")
        let shipping_longitude = string(dictAddress, "longitude")
        
        let dictShipping_details = NSMutableDictionary()
        
        dictShipping_details.setValue(shipping_name, forKey: "shipping_name")
        dictShipping_details.setValue(shipping_mobile, forKey: "shipping_mobile")
        dictShipping_details.setValue(shipping_city, forKey: "shipping_city")
        dictShipping_details.setValue(shipping_address, forKey: "shipping_address")
        dictShipping_details.setValue(shipping_state, forKey: "shipping_state")
        dictShipping_details.setValue(shipping_country, forKey: "shipping_country")
        dictShipping_details.setValue(shipping_pincode, forKey: "shipping_pincode")
        dictShipping_details.setValue(shipping_latitude, forKey: "shipping_latitude")
        dictShipping_details.setValue(shipping_longitude, forKey: "shipping_longitude")
        
        dictJson.setValue(dictShipping_details, forKey: "shipping_details")
        //other
       
       // var rewardPoints = Float()
        
        let SHIPPING_CHARGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "SHIPPING_CHARGE"))"
        let GST_PERCENTAGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "GST_PERCENTAGE"))"
        
     //   let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
        
       // let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
 
       // let user_reward_points =  string(dictInfo, "user_reward_points").count
        
      //  rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
        
        dictJson.setValue("", forKey: "txnid")
        dictJson.setValue("cod", forKey: "shipping_method")
        dictJson.setValue("", forKey: "paid")
        dictJson.setValue(commentText, forKey: "comment")
        dictJson.setValue("", forKey: "sub_total")
        dictJson.setValue(SHIPPING_CHARGE, forKey: "shipping")
      //  dictJson.setValue(used_reward_points, forKey: "used_reward_points")
        print("intRewardValue_---->>\(intRewardValue)")
        
        dictJson.setValue(intRewardValue, forKey: "used_reward_points")
        
        dictJson.setValue("0", forKey: "get_reward_points")
        dictJson.setValue(floatTotalAmount, forKey: "total")
        dictJson.setValue("1", forKey: "order_status_id")
        dictJson.setValue(shipping_latitude, forKey: "latitude")
        dictJson.setValue(shipping_longitude, forKey: "longitude")
        dictJson.setValue(GST_PERCENTAGE, forKey: "GST_PERCENTAGE")
        self.WS_ORDER_CREATE()
    }
 
    //MARK:- Add to cart
    
    func WS_ORDER_CREATE() {
        let arrBlank = NSMutableArray()
        
        PredefinedConstants().storeVeg____SynData(str: arrBlank)
        PredefinedConstants().storeFruits____SynData(str: arrBlank)
        ///////
        print("json dict>>\(dictJson.convertToString())")
        
        //////////////////////////////
        
        let params = NSMutableDictionary()
        params["order_json"] = dictJson.convertToString()
        
        Http.instance().json(WebServices().WS_ORDER_CREATE, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    Http.alert("", string(json! , "message"))
                    
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:- Rewards cell
   func btnMinus(intIndex: Int){
        print("btnPlus")
        print(intRewardValue)
        if intRewardValue !=  0{
            intRewardValue -= 1
            tblView.reloadData()
        }
    }
    
    func btnPlus(intIndex: Int){
        
        print(intRewardValue)
        
        let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        let user_reward_points =  string(dictInfo, "user_reward_points").count
        var rewardPoints = Float()
        
        rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
        
        if intRewardValue !=  Int(rewardPoints){
            intRewardValue += 1
            tblView.reloadData()
        }else{
            Http.alert("", "Maximum Points added.")
        }
    }
    
    func btnReward(intIndex: Int){
        intRewardValue = 0
        
        print("btnReward")
        if titleRewardButton == "Apply Rewards"{
           titleRewardButton = "Set Rewards"
        }else if titleRewardButton == "Set Rewards"{
            titleRewardButton = "Change Rewards"
        }else if titleRewardButton == "Change Rewards"{
           titleRewardButton = "Set Rewards"
        }else{
            
        }
         tblView.reloadData()
    }
     //==============================
    //MARK:- TEXTVIEW_DELEGATE *-*-*-*-*-*-*-*-*
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "CheckoutInstructionCell") as! CheckoutInstructionCell
        let index = IndexPath(row: 0, section: 5)
        
       // cell.lblPlaceholder.isHidden = true
        cell.lblPlaceholder.backgroundColor = UIColor.green
        cell.lblPlaceholder.text = ""
        cell.tvInstruction.becomeFirstResponder()
        tblView.scrollToRow(at: index, at: .top, animated: true)
        let rectOfCellInTableView = tblView.rectForRow(at: index)
        let rectOfCellInSuperview = tblView.convert(rectOfCellInTableView, to: tblView.superview)
        tblView.scrollRectToVisible(rectOfCellInSuperview, animated: true)
        tblView.setContentOffset(CGPoint(x: rectOfCellInSuperview.origin.x, y: rectOfCellInSuperview.origin.y + 35), animated: true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let cell = tblView.dequeueReusableCell(withIdentifier: "CheckoutInstructionCell") as! CheckoutInstructionCell
       // cell.lblPlaceholder.isHidden = true
        cell.lblPlaceholder.text = ""
        commentText = cell.tvInstruction.text
        if (text == "\n") {
            if cell.tvInstruction.text == "" {
              // cell.lblPlaceholder.isHidden = false
                 cell.lblPlaceholder.text = "Your Instructions"
                
            } else {
               // cell.lblPlaceholder.isHidden = true
                cell.lblPlaceholder.text = ""
            }
            cell.tvInstruction.resignFirstResponder()
            //  scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.view.endEditing(true)
           // tblView.scrollToRow(at: index, at: .top, animated: true)
           // tblView.scrollsToTop = true
            return false
        }
        
        var maxCharacter = Int()
        maxCharacter = 250
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }
 
    //======================

    
}//Class Ends Here ===========================.....Neeleshwari

class ProductsCell :UITableViewCell{
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//CheckOutAddressCell
public protocol CheckOutAddressCellDelegate{
    func btnCheckCircle(intIndex: Int)
}

class CheckOutAddressCell :UITableViewCell{
    
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnChecked: UIButton!
    @IBOutlet var imgCheckCircle: UIImageView!
    //Variables
    
    var delegateCheckOutAddressCell:CheckOutAddressCellDelegate?
    var intIndex = Int()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckedAction(_ sender: Any) {
        delegateCheckOutAddressCell?.btnCheckCircle(intIndex: intIndex)
    }
}


//MARK:- CheckoutRewardCell

public protocol CheckoutRewardCellDelegate{
    func btnPlus(intIndex: Int)
    func btnMinus(intIndex: Int)
    func btnReward(intIndex: Int)
}

class CheckoutRewardCell: UITableViewCell {
    
    @IBOutlet var btnRewards: UIButton!
    @IBOutlet var lblYourRewards: UILabel!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var lblRewardValue: UILabel!
    @IBOutlet var viewBGRewards: UIView!
    
    @IBOutlet var viewBGRewardsConstraintHeight: NSLayoutConstraint!
    
    //Variables
    
    var delegateCheckoutRewardCell:CheckoutRewardCellDelegate?
    var intIndex = Int()
    
    
    
    @IBAction func btnReward(_ sender: Any) {
        delegateCheckoutRewardCell?.btnReward(intIndex: intIndex)
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        delegateCheckoutRewardCell?.btnPlus(intIndex: intIndex)
    }
    
    
    @IBAction func btnMinusAction(_ sender: Any) {
        delegateCheckoutRewardCell?.btnMinus(intIndex: intIndex)
    }
    
}

class CheckoutInstructionCell: UITableViewCell {
    
    @IBOutlet var tvInstruction: JVFloatLabeledTextView!
    @IBOutlet var lblPlaceholder: UILabel!
    
}


class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat,_ left: CGFloat,_ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}




