//
//  AddAddressVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 17/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//
import UIKit
import Foundation
import GoogleMaps
import CoreLocation
import GooglePlacePicker
import JVFloatLabeledTextField

class AddAddressVC: UIViewController , UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource , UIGestureRecognizerDelegate , UITextViewDelegate, CLLocationManagerDelegate  {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var imgBG: ImageView!
    @IBOutlet var tfName: SkyFloatingLabelTextField!
    @IBOutlet var tfMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet var tfZipCode: SkyFloatingLabelTextField!
    //@IBOutlet var tfAddress: SkyFloatingLabelTextField!
    
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var btnState: UIButton!
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var btnAddAddress: UIButton!
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var pkrView: UIPickerView!
    @IBOutlet weak var btnDonePkr: UIButton!
    @IBOutlet weak var btnCancelPkr: UIButton!
    @IBOutlet var lblCountry: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var imgArrowCountry: UIImageView!
    @IBOutlet var imgArrowState: UIImageView!
    @IBOutlet var imgArrowCity: UIImageView!
    @IBOutlet var viewMainPicker: UIView!
    @IBOutlet var viewBGAddress: UIView!
    
    @IBOutlet var imgAddressMap: UIImageView!
    @IBOutlet var tvAddress: JVFloatLabeledTextView!
    //MARK:- Variables
    var arrPickerData = NSMutableArray()
    //var addressCoordinates = CLLocationCoordinate2D()
    var city_id = ""
    var state_id = ""
    var country_id = ""
    var dictAddress = NSDictionary()
    var addressLatitude = Double()
    var addressLongitude = Double()
    
    var locationManager: CLLocationManager!
    var currentLatitude: Double!
    var currentLongitude: Double!
    var placePicker: GMSPlacePicker!
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.addDoneButtonOnKeyboard()
        
        self.hideKeyboard()
        self.removePickerView()
        
    //    tfAddress.isUserInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if dictAddress.count == 0{
            setNavVegiDeliveryGreen(className: "ADD ADDRESS")
            btnAddAddress.setTitle("ADD", for: .normal)
            
            country_id = "101"
            state_id = "21"
            city_id = "2229"
            
            lblCountry.text = "India"
            lblState.text = "Madhya Pradesh"
            lblCity.text = "Indore"
            
            let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
            
            tfName.text = string(dictInfo, "first_name") + " " + string(dictInfo, "last_name")
            //tfEmail.text = string(dictInfo, "email")
            tfMobileNumber.text = string(dictInfo, "contact_number")
          //  tfDateOfBirth.text = string(dictInfo, "dob")
            
        }else{
            setNavVegiDeliveryGreen(className: "UPDATE ADDRESS")
            btnAddAddress.setTitle("UPDATE", for: .normal)
            self.setdata()
        }
        
        self.menuNavigationButton()
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        tfName.delegate = self
        tfMobileNumber.delegate = self
        tfZipCode.delegate = self
        tvAddress.delegate = self
        pkrView.delegate = self
        btnAddAddress.border(UIColor.clear, 22, 0)
        
        let userIconName = UIImage(named: "asuser")
        setPaddingWithRightImage(image: userIconName!, textField: tfName)
        
        let userIconMobile = UIImage(named: "mobile")
        setPaddingWithRightImage(image: userIconMobile!, textField: tfMobileNumber)
        
        let userIconZipcode = UIImage(named: "zipcode")
        setPaddingWithRightImage(image: userIconZipcode!, textField: tfZipCode)
        
        btnCountry.addBottomBorder(color: UIColor.black, height: 1)
        btnState.addBottomBorder(color: UIColor.black, height: 1)
        btnCity.addBottomBorder(color: UIColor.black, height: 1)
        viewBGAddress.addBottomBorder(color: appColor.appThemeGreenColor, height: 1)
        
        //
        btnCountry.setTitle("", for: .normal)
        btnState.setTitle("", for: .normal)
        btnCity.setTitle("", for: .normal)
        self.setDefauldPickerAddress()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }

    }
    
    func setDefauldPickerAddress()  {
       
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setdata()  {
        
        tfName.text = string(dictAddress, "name")
        tfMobileNumber.text = string(dictAddress, "mobile_number")
        tfZipCode.text = string(dictAddress, "pincode")
      //  tfAddress.text = string(dictAddress, "address")
        tvAddress.text = string(dictAddress, "address")
        
        lblCountry.text = string(dictAddress, "country_name")
        lblState.text = string(dictAddress, "state_name")
        lblCity.text = string(dictAddress, "city_name")
        
        if string(dictAddress, "country_name").count == 0 || string(dictAddress, "state_name").count == 0||string(dictAddress, "city_name").count == 0{
          //  lblCountry.text = "Country"
           // lblState.text = "State"
           // lblCity.text = "City"
            country_id = "101"
            state_id = "21"
            city_id = "2229"
            
            lblCountry.text = "India"
            lblState.text = "Madhya Pradesh"
            lblCity.text = "Indore"
        }
        
        addressLatitude = Double(string(dictAddress, "latitude"))!
        addressLongitude = Double(string(dictAddress, "longitude"))!
    }
    override func viewDidAppear(_ animated: Bool) {
        country_id = "101"
        state_id = "21"
        city_id = "2229"
        
        lblCountry.text = "India"
        lblState.text = "Madhya Pradesh"
        lblCity.text = "Indore"
    }
    
    
    //MARK:- Button Actions
    
    @IBAction func btnPickAddress(_ sender: Any) {
    // self.placeAutocomplete()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
           
            self.placePickerWork()
            
        } else {
            print("Location services are not enabled")
            //showUserSettings()
            Http.alert("", "Kindly enabled Location Services.")
        }
    }
    
    func showUserSettings() {
        guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        UIApplication.shared.open(urlGeneral)
    }
    
    @IBAction func btnCountryAction(_ sender: Any) {
        self.WS_Country()
        self.view.endEditing(true)
        btnCountry.isSelected = true
        btnState.isSelected = false
        btnCity.isSelected = false
    }
    
    @IBAction func btnStateAction(_ sender: Any) {
        
        self.view.endEditing(true)
        if lblCountry.text == "Country" {
            Http.alert("", "Country")
            
        }else{
            btnCountry.isSelected = false
            btnState.isSelected = true
            btnCity.isSelected = false
            self.WS_State()
        }
    }
    
    @IBAction func btnCityAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if lblCountry.text == "Country" {
            Http.alert("", "Country")
            
        }else if lblState.text == "State" {
            Http.alert("", "State")
            
        }else{
            
            btnCountry.isSelected = false
            btnState.isSelected = false
            btnCity.isSelected = true
            self.WS_City()
        }
    }
    
    @IBAction func btnAddAddressAction(_ sender: Any) {
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.WS_AddAddress()
        }
    }
    
    //MARK:- MAP WORK
    //MARK: Location protocol
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        // 1
        let location:CLLocation = locations.last!
        self.currentLatitude = location.coordinate.latitude
        self.currentLongitude = location.coordinate.longitude
        
        // 2
        let coordinates = CLLocationCoordinate2DMake(self.currentLatitude, self.currentLongitude)
        print("current coordinates>>>\(coordinates)")
        
     //   let marker = GMSMarker(position: coordinates)
      //  marker.title = "I am here"
       // marker.map = self.googleMapView
       // self.googleMapView.animate(toLocation: coordinates)

    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error){
        
        print("An error occurred while tracking location changes : \(error)")
    }
    //////==============

    
    func placePickerWork()  {
        
        Http.startActivityIndicator()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            Http.stopActivityIndicator()
        })
        // 1
        // let center = CLLocationCoordinate2DMake(self.latitude, self.longitude)
        let center = CLLocationCoordinate2DMake(72.055477, 27.547845)
        
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        
        // 2
        
        placePicker.pickPlace { (place: GMSPlace?, error: Error?) -> Void in
            
            if let error = error {
                print("Error occurred: \(error.localizedDescription)")
                return
            }
            // 3
            if let place = place {
               let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                
             //   self.placePickedCoordinate = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
            //self.getAddressFromGeocodeCoordinate(coordinate: coordinates)
        self.getAddressFromLatLon(pdblLatitude: "\(place.coordinate.latitude)", withLongitude: "\(place.coordinate.longitude)")
                
               // let marker = GMSMarker(position: coordinates)
               // marker.title = place.name
               // marker.map = self.googleMapView
                //self.googleMapView.animate(toLocation: coordinates)
                
            } else {
                print("No place was selected")
            }
        }
    }
    
    func getAddressFromGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            
             print("response>>\(response)")
            //Add this line
            if let address = response!.firstResult() {
                //let lines = address.lines! as [String]
               // print(lines)
                print("address>>\(address)")
            }
        }
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                   // print(pm.country)
                  //  print(pm.locality)
                   // print(pm.subLocality)
                   // print(pm.thoroughfare)
                   // print(pm.postalCode)
                   // print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                    self.tvAddress.text = "\(addressString)"
                }
        })
        
    }
    
    var placePickedCoordinate = CLLocationCoordinate2D()
    
    //=============================
    func placeAutocomplete()  {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
     //================
    //MARK: PICKER_BUTTONS.
    
    @IBAction func btnCancelPkr(_ sender: Any) {
        self.removePickerView()
    }
    
    @IBAction func btnDonePkr(_ sender: Any) {
        
        let dictPkr = arrPickerData.object(at: pkrView.selectedRow(inComponent: 0)) as! NSDictionary
        let strName = string(dictPkr, "name")
        
        
        if btnCountry.isSelected {
            country_id = string(dictPkr, "country_id")
            lblCountry.text = strName
            lblState.text = "State"
            lblCity.text = "City"
            
        }else if btnState.isSelected {
            
            state_id = string(dictPkr, "state_id")
            lblState.text = strName
            lblCity.text = "City"
            
        } else if btnCity.isSelected {
            
            city_id = string(dictPkr, "city_id")
            //btnCity.setTitle(strName, for: .normal)
            lblCity.text = strName
        }
        self.removePickerView()
    }
    
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if tfName.text?.count == 0  {
            return AlertMSG.blankName
            
        }else if  tfMobileNumber.text?.count == 0  {
            return AlertMSG.blankMobile
            
        } else if (tfMobileNumber.text?.count)! < 10 {
            return AlertMSG.invalidMobile
            
      //  }else if  tfZipCode.text?.count == 0  {
       //     return AlertMSG.blankZipCode
            
        } else if tfZipCode.text?.count != 0 && (tfZipCode.text?.count)! < 6 {
            return AlertMSG.invalidZipCode
            
        } else if tvAddress.text?.count == 0 {
            return AlertMSG.blankAddress
            
        } else if btnCountry.title(for: .normal) == "Country" {
            return AlertMSG.blankCountry
            
        } else if btnState.title(for: .normal) == "State" {
            return AlertMSG.blankState
            
        }else if btnCity.title(for: .normal) == "City" {
            return AlertMSG.blankCity
        }
        
        return nil
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        // scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.removePickerView()
        /*
        if textField == tfAddress{
            self.view.endEditing(true)
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
        */
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfName {
            tfMobileNumber.becomeFirstResponder()
        }else if textField == tfMobileNumber {
            tfZipCode.becomeFirstResponder()
        }else if textField == tfZipCode {
            tvAddress.becomeFirstResponder()
        }else if textField == tvAddress {
            //tfAddress.resignFirstResponder()
            tvAddress.resignFirstResponder()
           // scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfName {
            return (length > 40) ? false : true
        }else if textField == tfMobileNumber {
            return (length > 10) ? false : true
        }
        else if textField == tfZipCode {
            return (length > 9) ? false : true
        }
            /*
        else if textField == tfAddress {
            return (length > 50) ? false : true
        }
        */
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnAddAddress.frame.origin.y + btnAddAddress.frame.size.height + 10)
        self.hideKeyboard()
        
    }
    
    //MARK:- TEXTVIEW_DELEGATE
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
       /*
        if UIScreen.main.bounds.size.height < 667{
            if (textView.frame.origin.y >= 180) {
                scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textView.frame.origin.y - 220))) , animated: true)
            }
        }
 */
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var maxCharacter = Int()
        maxCharacter = 250
        
        if (text == "\n") {
            tvAddress.resignFirstResponder()
            self.hideKeyboard()
            
          //  scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            //  scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
            
            //            if textView.text.count == 0{
            //                textView.title
            //            }
            //
            
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }

    ///MARK:- AddDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfMobileNumber.inputAccessoryView = doneToolbar
        self.addDoneForZippedCode()
    }
    
    func addDoneForZippedCode()  {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        // let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        // items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfZipCode.inputAccessoryView = doneToolbar
    }
    
    @objc func nextOfDoneTool() {
        if tfMobileNumber.isFirstResponder{
            tfZipCode.becomeFirstResponder()
        }
        else{
            tvAddress.becomeFirstResponder()
        }
        
    }
    
    func done() {
        self.view.endEditing(true)
        // scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    //MARK: PICKERVIEW DELEGATES
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var strName = ""
        
        if let dict = arrPickerData.object(at: row) as? NSDictionary {
            strName = string(dict, "name")
        }
        
        // strName = arrPickerData.object(at: row) as! String
        
        return strName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func removePickerView() {
        //   viewPicker.hideViewToBottom()
        viewPicker.isHidden = true
       // removePickerView(bottomView: viewPicker)
        removeSubViewWithAnimation(viewMain: viewMainPicker, viewPopUP: viewPicker)
    }
    
    func addPickerView() {
        viewPicker.isHidden = false
        self.view.endEditing(true)
        // viewPicker.pullViewFromBottom()
        //  addPickerView(bottomView: viewPicker)
        
        pkrView.reloadAllComponents()
        pkrView.selectRow(0, inComponent: 0, animated: true)
        
       // addPickerView(bottomView: viewPicker)
         addPopupShowVerification(viewMain: viewMainPicker, viewPopUP: viewPicker)
    }
    
    //================================
    
    //MARK:- FUNCTION FOR POPVIEW///================
    func addPopupShowVerification(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewMain)
        //self.scroll.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self
        viewMain.addGestureRecognizer(tap)
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        
        UIView.animate(withDuration: Double(0.3), delay: 0, options: [.curveEaseIn], animations: {
            
            viewTest.frame.origin.y = self.view.frame.size.height - viewTest.frame.size.height
        }, completion: {(success) in
        })
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        
        UIView.animate(withDuration: Double(0.2), animations: {
            viewPopUP.frame.origin.y = PredefinedConstants.ScreenHeight - viewPopUP.frame.size.height
            
        }, completion: {(success) in
            viewMain.removeFromSuperview()
            viewPopUP.frame.origin.y = PredefinedConstants.ScreenHeight + viewPopUP.frame.size.height
        })
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewMainPicker, viewPopUP: viewPicker)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewMainPicker.bounds.contains(touch.location(in: viewPicker)) {
            return false
        }
        
        return true
    }
    
    //================================
    
    ///////////////////////////////////////////
    
    //  //MARK:- ----------- get WS_Country
    func WS_Country() {
        
        let params = NSMutableDictionary()
        
        Http.instance().json(WebServices().COUNTRY_LIST, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrPickerData = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        self.arrPickerData  = arr.mutableCopy() as! NSMutableArray
                        
                        self.pkrView.selectRow(0, inComponent: 0, animated: true)
                        self.addPickerView()
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //  //MARK:- -----------WS_State
    func WS_State() {
        
        let params = NSMutableDictionary()
        
        let ws_state = "\(WebServices().STATE_LIST)/id/\(country_id)"
        print("ws_state>>\(ws_state)")
        
        Http.instance().json(ws_state, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrPickerData = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        self.arrPickerData  = arr.mutableCopy() as! NSMutableArray
                        print("arr>>>\(arr)")
                        self.pkrView.selectRow(0, inComponent: 0, animated: true)
                        self.addPickerView()
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //  //MARK:- -----------WS_City
    func WS_City() {
        
        let params = NSMutableDictionary()
        // params["state_id"] = state_id
        print("state_id>>\(state_id)")
        
        let ws_city = "\(WebServices().CITY_LIST)/id/\(state_id)"
        
        print("ws_city>>\(ws_city)")
        Http.instance().json(ws_city , params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrPickerData = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        self.arrPickerData  = arr.mutableCopy() as! NSMutableArray
                        print("arr>>>\(arr)")
                        self.pkrView.selectRow(0, inComponent: 0, animated: true)
                        self.addPickerView()
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:- ------------------ WS_Add Address ----------------------//
    func WS_AddAddress() {
        
        let params = NSMutableDictionary()
        params["name"] = tfName.text
        params["mobile_number"] = tfMobileNumber.text
        params["address"] = tvAddress.text//tfAddress.text
        params["city"] = city_id//lblCity.text
        params["state"] = state_id//lblState.text
        params["country"] = country_id//lblCountry.text
        params["pincode"] = tfZipCode.text
        params["latitude"] = addressLatitude
        params["longitude"] = addressLongitude
        
        var ws = ""
        
        if dictAddress.count == 0{
            ws = WebServices().WS_AddAddress
        }else{
            ws = WebServices().WS_EditAddress
            params["address_id"] = string(dictAddress, "address_id")
        }
        
        Http.instance().json(ws, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    Http.alert("", string(json! , "message"))
                    
                    self.tfName.text = ""
                    self.tfMobileNumber.text = ""
                    self.tfZipCode.text = ""
                   // self.tfAddress.text = ""
                    self.tvAddress.text = ""
                   
                    self.arrPickerData = NSMutableArray()
                      _ = self.navigationController?.popViewController(animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//Class Ends Here ===========================.....Neeleshwari

extension UIView {
    func addBottomBorder(color: UIColor, height: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
                                                attribute: NSLayoutAttribute.height,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: nil,
                                                attribute: NSLayoutAttribute.height,
                                                multiplier: 1, constant: height))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutAttribute.bottom,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self,
                                              attribute: NSLayoutAttribute.bottom,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutAttribute.leading,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self,
                                              attribute: NSLayoutAttribute.leading,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutAttribute.trailing,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self,
                                              attribute: NSLayoutAttribute.trailing,
                                              multiplier: 1, constant: 0))
    }
    
}

//MARK:- Address Picker

extension AddAddressVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //  print("Place name: \(place.name)")
        //  print("Place address: \(place.formattedAddress!)")
        
        tvAddress.text = place.formattedAddress!
        
        addressLatitude = place.coordinate.latitude
        addressLongitude = place.coordinate.longitude
        
        //print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}





