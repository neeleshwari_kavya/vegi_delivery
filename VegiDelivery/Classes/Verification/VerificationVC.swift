//
//  VerificationVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 11/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit
import PinCodeTextField

class VerificationVC : UIViewController , UIGestureRecognizerDelegate , PinCodeTextFieldDelegate{
    
    @IBOutlet var scrolView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var viewPinCode: PinCodeTextField!
    @IBOutlet var btnVerify: UIButton!
    @IBOutlet var btnResend: UIButton!
    
    //Mark:- Variables
   // var verificationCode = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scrolView.contentInset = UIEdgeInsets.zero
        self.BackNavigationButton()
        setNavTransparent(className: "MOBILE VERIFICATION")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            self.viewPinCode.becomeFirstResponder()
        }
        
        viewPinCode.delegate = self
        viewPinCode.keyboardType = .numberPad
        
        /////////////////////////////////////////////////
        btnVerify.border(UIColor.clear, 22, 0)
        self.addDoneButtonOnKeyboard()
    }
    
    func addNextButton()  {
        
        let toolbar = UIToolbar()
        let nextButtonItem = UIBarButtonItem(title: NSLocalizedString("NEXT",
                                                                      comment: ""),
                                             style: .done,
                                             target: self,
                                             action: #selector(pinCodeNextAction))
        toolbar.items = [nextButtonItem]
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        viewPinCode.inputAccessoryView = toolbar
    }
    
    ///addDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = #colorLiteral(red: 0.1609999985, green: 0.6079999804, blue: 0.2820000052, alpha: 1)
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        // let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.done))
        
        var items:[UIBarButtonItem] = []
        // items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.viewPinCode.inputAccessoryView = doneToolbar
    }
    
    @objc func done() {
        viewPinCode.resignFirstResponder()
    }
    
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "back-arrow"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func pinCodeNextAction() {
        viewPinCode.resignFirstResponder()
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnResendAction(_ sender: Any) {
        self.view.endEditing(true)
        self.WS_Resend_Code()
    }
    
    @IBAction func btnVerifyAction(_ sender: Any) {
        self.viewPinCode.resignFirstResponder()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            WS_Verification()
        }
        
        
    }
    
    func checkValidation() -> String? {
        
        if viewPinCode.text?.count == 0 {
            return AlertMSG.blankVerificationCode
            
        }else if viewPinCode.text?.count != 6 {
            
            return AlertMSG.invalidVerificationCode
        }
        
        return nil
    }
    
    //MARK:- Delegates Methoda PIN code View
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    //MARK:- WS_Resend_Code
    func WS_Resend_Code(){
        
        let params = NSMutableDictionary()
       
        Http.instance().json(WebServices().WS_Resend_Code, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken() ,nil) { (json, params) in
            let json = json as? NSDictionary
            if (json != nil) {
                if number(json!, "success") == 1 {
                     Http.alert(string(json!, "message"), "")
                    print("code sent ")
                 
                } else {
                    Http.alert(string(json!, "message"), "")
                }
            }else {
                
                 Http.alert(string(json!, "message"), "")
            }
        }
    }
    
    
    //MARK:- WS_Verification
    func WS_Verification() {
        
        let params = NSMutableDictionary(dictionary:[
            "verification_code":viewPinCode.text!
            ])
        
        Http.instance().json(WebServices().WS_Verification_Code, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken() ,nil) { (json, params) in
            let json = json as? NSDictionary
            if (json != nil) {
                if number(json!, "success") == 1 {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVendor") as! RootVendor
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    Http.alert(string(json!, "message"), "")
                }
            }else {
                
                Http.alert(string(json!, "message"), "")
            }
        }
    }
    
}//Class Ends Here ===========================.....Neeleshwari








