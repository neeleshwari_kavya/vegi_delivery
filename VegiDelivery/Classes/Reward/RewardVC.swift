//
//  RewardVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 13/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class RewardVC: UIViewController  {
    
    //MARK:- Variables
    
    @IBOutlet var viewCircle: UIView!
    @IBOutlet var lblAvailableBalance: UILabel!
    
    @IBOutlet var lblWalletHistory: UILabel!
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "REWARD")
        self.menuNavigationButton()
        self.WS_Rewards_History()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        viewCircle.layer.cornerRadius = viewCircle.frame.size.height/2
        viewCircle.layer.borderWidth = 4
        
        viewCircle.layer.borderColor = UIColor.lightGray.cgColor
        lblWalletHistory.font = UIFont.boldSystemFont(ofSize: 25)
        lblWalletHistory.text = "Wallet History"
        
        let totalBalance = "₹0.00"
        let staticText = "\nAvailable Balance"
        
        lblAvailableBalance.attributedText =  getMyAtrributedString(strFirst:totalBalance, fontColorFirst:UIColor.black , strSecond:staticText, fontColorSecond:UIColor.black)
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        //  _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Fuctions NSMutableAttributedString
    
    func getMyAtrributedString(strFirst:String, fontColorFirst:UIColor , strSecond:String, fontColorSecond:UIColor) -> NSMutableAttributedString {
        
        let attributedStr1 = [
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 15.0),
            NSAttributedStringKey.foregroundColor : fontColorFirst]
        
        let attributedStr2 = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0),
            NSAttributedStringKey.foregroundColor : fontColorSecond]
        
        let myFinalString1 = NSMutableAttributedString(string: strFirst, attributes: attributedStr1 )
        
        let myFinalString2 = NSMutableAttributedString(string: strSecond, attributes: attributedStr2 )
        
        myFinalString1.append(myFinalString2)
        
        return myFinalString1
    }
    
    
    //MARK: WS TermAndConditions Authtoken
    func WS_Rewards_History() {
        
        let params = NSMutableDictionary()
     
        Http.instance().json(WebServices().WS_Rewards_History, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                  
                    let user_reward_points = string(json! , "user_reward_points")
                    
                   // print("user_reward_points...>>\(user_reward_points)")
                    
                    let totalBalance =  String(format: "₹ %.2f", Float(user_reward_points)!)
                    
                    //print("totalBalance>>\(totalBalance)")
                    
                    let staticText = "\nAvailable Balance"
                    self.lblAvailableBalance.attributedText =  self.getMyAtrributedString(strFirst:"\(totalBalance)", fontColorFirst:UIColor.black , strSecond:staticText, fontColorSecond:UIColor.black)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
    
    
}//Class Ends Here ===========================.....Neeleshwari


