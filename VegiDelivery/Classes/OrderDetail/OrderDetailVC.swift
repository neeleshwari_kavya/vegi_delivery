
//
//  OrderDetailVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 20/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class OrderDetailVC: UIViewController , UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate , UITextViewDelegate{
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblOrderId: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblShippingMethod: UILabel!
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var viewStatusHeader: UIView!
    @IBOutlet var btnCancelOrder: UIButton!
    
    //cancellation pop up
    @IBOutlet var viewCancelOrder: UIView!
    @IBOutlet var viewPopUPCancelOrder: View!
    @IBOutlet var btnClosePopUP: UIButton!
    @IBOutlet var lblReason: UILabel!
    @IBOutlet var tvReason: JVFloatLabeledTextView!
    @IBOutlet var btnSubmitReason: UIButton!
    @IBOutlet var viewButtonCancel: UIView!
    @IBOutlet var viewButtonCancelConstraintHeight: NSLayoutConstraint!
    
    //MARK:- Variables
    var arrOrders = NSMutableArray()
    var order_id = ""
    var floatTotalAmount = Float()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.tblView.tableHeaderView =  viewStatusHeader
        self.tblView.frame.origin.y = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "ORDER DETAIL")
        // self.menuNavigationButton()
      
        if PredefinedConstants.appDelegate.menuClass == "menu"{
            menuNavigationButton()
            //var order_id = ""
            order_id =  string(kAppDelegate.dictCatchNotifications, "order_id")
            
            print("order_id>>\(order_id)")
            self.ws_MyOrderDetail()
        }else{
            menuNavigationBackButton()
            self.ws_MyOrderDetail()
        }
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        PredefinedConstants.appDelegate.menuClass = ""
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func BTNBACK(_ sender: Any) {
       //  _ = self.navigationController?.popViewController(animated: true)
    }
    
    func menuNavigationBackButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
       // _ = self.navigationController?.popViewController(animated: true)
        
        if PredefinedConstants.appDelegate.menuClass == "menu"{
            self.sideMenuViewController.presentLeftMenuViewController()
        
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
        tvReason.delegate = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        btnCancelOrder.border(UIColor(named: "redColor"), 20, 1.0)
        // tvReason.border(UIColor.gray, 5, 1.0)
    }

    var arrCartList = NSMutableArray()
    
    func setOrderDetails()  {
        
        let  id = order_id
        let shipping_method = string(dictOrderDetails, "shipping_method")
        let updated_at = string(dictOrderDetails, "updated_at").converDate("yyyy-MM-dd HH:mm:ss", "MMM d yyyy, hh:mm a")
        
        lblOrderId.attributedText = getMyAtrributedString(strFirst: "OrederID: ", fontColorFirst: UIColor(named: "redColor")!, strSecond:  id, fontColorSecond: UIColor.black)
        
        lblDate.attributedText = getMyAtrributedString(strFirst: "Date: ", fontColorFirst: UIColor(named: "redColor")!, strSecond:  updated_at, fontColorSecond: UIColor.black)
        
        lblShippingMethod.attributedText = getMyAtrributedString(strFirst: "Shipping Method: ", fontColorFirst: UIColor(named: "redColor")!, strSecond:  shipping_method, fontColorSecond: UIColor.black)
       
        let order_status_id = string(dictOrderDetails, "order_status_id")
        
        if order_status_id == "1"{
            lblStatus.textColor = appColor.pending
            lblStatus.text = "Pending"
            imgStatus.image = UIImage(named: "pending")
        }else if order_status_id == "2"{
            lblStatus.textColor = appColor.inprocess
           lblStatus.text = "Inprocess"
            imgStatus.image = UIImage(named: "inprocess")
        }else if order_status_id == "3" {
            lblStatus.textColor = appColor.dispatched
            lblStatus.text = "Dispatched"
            imgStatus.image = UIImage(named: "dispatched")
        }else if order_status_id == "4"{
           lblStatus.textColor = appColor.completed
           lblStatus.text = "Delivered"
            imgStatus.image = UIImage(named: "completed")
        }else if order_status_id == "5"{
            lblStatus.textColor = appColor.cancel
            lblStatus.text = "Cancelled"
            imgStatus.image = UIImage(named: "cancelled")
            viewButtonCancelConstraintHeight.constant = 0
        }else{
            lblStatus.textColor = appColor.pending
              lblStatus.text = "Pending"
            imgStatus.image = UIImage(named: "pending")
        }
        
        tblView.reloadData()
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowVerification(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
         self.view.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
        tvReason.text = ""
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewCancelOrder, viewPopUP: viewPopUPCancelOrder)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewCancelOrder.bounds.contains(touch.location(in: viewPopUPCancelOrder)) {
            return false
        }
        
        return true
    }
    
    //MARK:- Fuctions NSMutableAttributedString
    
    func getMyAtrributedString(strFirst:String, fontColorFirst:UIColor , strSecond:String, fontColorSecond:UIColor) -> NSMutableAttributedString {
        
        let attributedStr1 = [
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 15.0),
            NSAttributedStringKey.foregroundColor : fontColorFirst]
        
        let attributedStr2 = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0),
            NSAttributedStringKey.foregroundColor : fontColorSecond]
        
        let myFinalString1 = NSMutableAttributedString(string: strFirst, attributes: attributedStr1 )
        
        let myFinalString2 = NSMutableAttributedString(string: strSecond, attributes: attributedStr2 )
        
        myFinalString1.append(myFinalString2)
        
        return myFinalString1
    }
    
    //MARK:- BUtton Actions
    
    @IBAction func btnCancelOrder(_ sender: Any) {
       addPopupShowVerification(viewMain: viewCancelOrder, viewPopUP: viewPopUPCancelOrder)
    }
    @IBAction func btnClosePopUP(_ sender: Any) {
        
        removeSubViewWithAnimation(viewMain: viewCancelOrder, viewPopUP: viewPopUPCancelOrder)
        
    }
    
    @IBAction func btnSubmitReason(_ sender: Any) {
        if let str = checkPasswordValidation() {
            Http.alert("", str)
        } else {
            self.WS_ORDER_CANCEL()
        }
    }
    
    func checkPasswordValidation() -> String? {
        
        if tvReason.text?.count == 0 {
            return AlertMSG.cancelOrderReason
        }
        return nil
    }

    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return arrCartList.count//arrOrders.count + 1
        }else if section == 1{
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tblView.dequeueReusableCell(withIdentifier: "OrdersDetailCell") as! OrdersDetailCell
            
            let dict = arrCartList.object(at: indexPath.row) as! NSDictionary
            
            cell.lblName.text = string(dict, "name")
            cell.lblPrice.text = "₹\(string(dict, "price"))"//"Price: ₹4545.20"
            cell.lblQuantity.text = string(dict, "quantity")
            
            let strOption = string(dict, "option")
            let dictOption = convertToDictionary(text: strOption)! as NSDictionary
            cell.lblWeight.text = string(dictOption , "weight")
            let amt = Float(number(dict, "price")) * Float(string(dict, "quantity"))!
           
            cell.lblTotalPrice.text = "₹\(amt)"///
            
        //    ===============================
            cell.activityIndicator.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                cell.activityIndicator.isHidden = true
            })
          //    ===============================
            //product image
            let UserImage_URLString = WebServices().WEB_URL
            let strUserImg = string(dict, "image")
            let imageUrl = UserImage_URLString + strUserImg
            
            if strUserImg.isEmpty {
                cell.imgOrder.image = UIImage(named: "vegi_default")
            } else {
                let url =  NSURL(string: imageUrl)
                // cell.imgProduct.af_setImage(withURL: url as URL!)
                cell.imgOrder.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
            }

            return cell
            
        }else if indexPath.section == 1{
            let cell = tblView.dequeueReusableCell(withIdentifier: "TaxDetailCell") as! TaxDetailCell
         
           //++++++++++++++++++++++++++++++++++++
            
            let SHIPPING_CHARGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "SHIPPING_CHARGE"))"
            let GST_PERCENTAGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "GST_PERCENTAGE"))"
            
         //   let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
          //  let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
         //   let user_reward_points =  string(dictInfo, "user_reward_points").count
            
         //   rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
            

            let gstPrice :Float = Float(GST_PERCENTAGE)! * PredefinedConstants.appDelegate.floatTotalPrice * 0.01
            
            cell.lblTotalAmount.text = String(format: "Total: ₹ %.2f", PredefinedConstants.appDelegate.floatTotalPrice)//"Total: ₹" + "\(floatTotalAmount)"//string(dictOrderDetails, "total")
            cell.lblGST.text = "Gst(+\(GST_PERCENTAGE)%): ₹ " + String(format: "%.2f", gstPrice) //"Gst(+\(GST_PERCENTAGE)%): ₹" + "\(gstPrice)"
            cell.lblShippingCharges.text =  String(format: "Shipping Charge(+): ₹ %.2f", Float(SHIPPING_CHARGE)!)//"Shipping Charge(+): ₹" + "\(SHIPPING_CHARGE)"
          
            let used_reward_points = string(dictOrderDetails, "used_reward_points")
            print("used_reward_points str>>\(used_reward_points)")
            
             var floatUsedPts = Float()
                floatUsedPts = Float(number(dictOrderDetails, "used_reward_points"))
            
             cell.lblRewards.text =  String(format: "Reward(-): ₹ %.2f", floatUsedPts)//"Reward(-): ₹" + "\(rewardPoints)"
       
            let finalTotal = floatTotalAmount +  Float(SHIPPING_CHARGE)! + gstPrice - floatUsedPts
            print("finalTotal---->>\(finalTotal)")
            
            cell.lblGrandTotal.text = String(format: "Grand Total: ₹ %.2f", finalTotal)//"Grand Total: ₹" + "\(finalTotal)"
              //++++++++++++++++++++++++++++++++++++
            
            return cell
            
        } else{
            let cell = tblView.dequeueReusableCell(withIdentifier: "shippingDetailCell") as! shippingDetailCell
            
            cell.lblName.text = string(dictOrderAddress, "shipping_name")
            cell.lblMobilenNumber.text = string(dictOrderAddress, "shipping_mobile")
            cell.lblEmailId.text = string(dictOrderDetails, "payment_email")
            cell.lblAddress.text = string(dictOrderAddress, "shipping_address")
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    //Mark:-tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var viewHeader = UIView()
        
        viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: 40))
        
        let lblSectionTitle = UILabel(frame: CGRect(x: 5, y: 0, width: self.tblView.frame.size.width - 40, height: 40))
        
        lblSectionTitle.font =  UIFont.systemFont(ofSize: 15, weight: .semibold)
        lblSectionTitle.textColor = UIColor.black
        lblSectionTitle.numberOfLines = 1
        
        if section == 0 {
            lblSectionTitle.text = "Items in Order"
            lblSectionTitle.textAlignment = .center
        }else if section == 1 {
            lblSectionTitle.text = ""
            lblSectionTitle.textAlignment = .left
        }else {
            lblSectionTitle.text = "Shipping Details"
            lblSectionTitle.textAlignment = .left
        }
        
        viewHeader.addSubview(lblSectionTitle)
        lblSectionTitle.backgroundColor = UIColor.clear
        viewHeader.backgroundColor = UIColor.clear
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1{
            return 0
        } else{
            return 40
        }
    }
    
    var dictOrderDetails = NSMutableDictionary()
    var dictOrderAddress = NSMutableDictionary()
    //MARK:- ------------------ ws_MyOrder List ----------------------//
    func ws_MyOrderDetail() {
        
        let params = NSMutableDictionary()
        params["order_id"] = order_id
        
        Http.instance().json(WebServices().WS_Order_Detail, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    if let result = json?.object(forKey: "result") as? NSDictionary {
                        if let dictOrder = result.object(forKey: "order") as? NSDictionary {
                            self.dictOrderDetails = dictOrder.mutableCopy() as! NSMutableDictionary
                        }
                        
                        if let dictAddress = result.object(forKey: "orderAddress") as? NSDictionary {
                            self.dictOrderAddress = dictAddress.mutableCopy() as! NSMutableDictionary
                        }
                        if let arr = result.object(forKey: "order_item") as? NSArray {
                            
                            self.arrCartList =  arr.mutableCopy() as! NSMutableArray
                          
                            print("arrCartList>>\(self.arrCartList)")
                            
                                self.totalAmount()
                            //self.tblView.reloadData()
                        }
                        self.setOrderDetails()
                    }
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //WS_ORDER_CANCEL==========////////////////////////
    func WS_ORDER_STATUS() {
        
        let params = NSMutableDictionary()
        params["order_id"] = order_id
        
        Http.instance().json(WebServices().WS_ORDER_STATUS, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                   // if let result = json?.object(forKey: "result") as? NSDictionary {
                    //}
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //WS_ORDER_CANCEL==========////////////////////////
    func WS_ORDER_CANCEL() {
        
        let params = NSMutableDictionary()
        params["order_id"] = order_id
        params["order_cancel_reason"] = tvReason
        
        Http.instance().json(WebServices().WS_ORDER_CANCEL, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                     Http.alert("", string(json! , "message"))

                    self.viewButtonCancelConstraintHeight.constant = 0
                    self.removeSubViewWithAnimation(viewMain: self.viewCancelOrder, viewPopUP: self.viewPopUPCancelOrder)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:- TEXTVIEW_DELEGATE
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        /*
        if UIScreen.main.bounds.size.height < 667{
            if (textView.frame.origin.y >= 180) {
               // scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textView.frame.origin.y - 220))) , animated: true)
            }
        }
        */
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var maxCharacter = Int()
        maxCharacter = 250
        
        if (text == "\n") {
            tvReason.resignFirstResponder()
            self.hideKeyboard()
          
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    //MARK:- ------------------ cart List  total price
    func totalAmount(){
         var floatTotalPrice = Float()
          floatTotalPrice = 0.00
        
        print("arrCartList>>\(arrCartList)")
        
        for i in 0..<arrCartList.count {
            
            let dict = arrCartList.object(at: i) as! NSDictionary
            
            let strOption = string(dict, "option")
            let dictOption = convertToDictionary(text: strOption)! as NSDictionary
            
            let amt = Float(number(dict, "price")) * Float(string(dict, "quantity"))!
            print("amt>>>\(amt)")
            floatTotalPrice += amt
        }
        
     //   lblTotalAmount.text = String(format: "₹ %.2f", floatTotalPrice)//"₹\(floatTotalPrice)"///
        
        print("floatTotalPrice>>>\(floatTotalPrice)")
        PredefinedConstants.appDelegate.floatTotalPrice = floatTotalPrice
    }
    
  
}//Class Ends Here ===========================.....Neeleshwari

class OrdersDetailCell :UITableViewCell{
    
    @IBOutlet var imgOrder: UIImageView!
    
    @IBOutlet var viewBG: View!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    //Variables
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class TaxDetailCell :UITableViewCell{
    
    @IBOutlet var viewTaxes: UIView!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblGST: UILabel!
    @IBOutlet var lblShippingCharges: UILabel!
    @IBOutlet var lblRewards: UILabel!
    @IBOutlet var lblGrandTotal: UILabel!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class shippingDetailCell :UITableViewCell{
    
    
    @IBOutlet var viewBG: View!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMobilenNumber: UILabel!
    @IBOutlet var lblEmailId: UILabel!
    @IBOutlet var lblAddress: UILabel!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}



