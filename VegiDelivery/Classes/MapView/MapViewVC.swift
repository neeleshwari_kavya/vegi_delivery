//
//  MapViewVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 10/05/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import GooglePlacePicker

class MapViewVC:  UIViewController, CLLocationManagerDelegate  {
    
    var locationManager: CLLocationManager!
    var placePicker: GMSPlacePicker!
    var latitude: Double!
    var longitude: Double!
    var googleMapView: GMSMapView!
    
    @IBOutlet var viewMapGMS: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      //  self.googleMapView = GMSMapView(frame: self.mapViewContainer.frame)
      
         self.googleMapView = GMSMapView(frame: self.viewMapGMS.frame)
        
        self.googleMapView.animate(toZoom: 18.0)
        self.view.addSubview(googleMapView)
       // googleMapView.delegate = self
        
    }
    
    func displayAndDelegates() {
        // Do any additional setup after loading the view, typically from a nib.
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    //MARK: Location protocol
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
         locationManager.stopUpdatingLocation()
        // 1
        let location:CLLocation = locations.last!
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        
        // 2
        let coordinates = CLLocationCoordinate2DMake(self.latitude, self.longitude)
        let marker = GMSMarker(position: coordinates)
        marker.title = "I am here"
        marker.map = self.googleMapView
        self.googleMapView.animate(toLocation: coordinates)
        
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error){
        
        print("An error occurred while tracking location changes : \(error)")
    }
    
    
    @IBOutlet var btnShowLocations: UIButton!
    
    
    
    @IBAction func btnShowLocations(_ sender: Any) {
        
        // 1
       // let center = CLLocationCoordinate2DMake(self.latitude, self.longitude)
        let center = CLLocationCoordinate2DMake(72.055477, 27.547845)
        
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        
        // 2
        
        placePicker.pickPlace { (place: GMSPlace?, error: Error?) -> Void in
            
            if let error = error {
                print("Error occurred: \(error.localizedDescription)")
                return
            }
            // 3
            if let place = place {
                let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                let marker = GMSMarker(position: coordinates)
                marker.title = place.name
                marker.map = self.googleMapView
                self.googleMapView.animate(toLocation: coordinates)
            } else {
                print("No place was selected")
            }
        }
    }
    
    
}







