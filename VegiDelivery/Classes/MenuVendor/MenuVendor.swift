//
//  LeftMenuView.swift
//  ResideMenuDemo
//
//  Created by Rahul Khatri on 24/07/17.
//  Copyright © 2017 KavyaSoftech. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications
import AudioToolbox
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

class MenuVendor: UIViewController, UITableViewDelegate, UITableViewDataSource ,UIGestureRecognizerDelegate ,UNUserNotificationCenterDelegate {
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewButton: UIView!
    @IBOutlet var btnLogout: Button!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var imgEditProfile: UIImageView!
    @IBOutlet var viewBgTop: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    //logout
    
    //MARK:- VARIABLES
    var menuViewWidth: CGFloat!
    var arrMenu = NSMutableArray()
    var selectindex:Int = 0
    let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuItems()
        let screenWidth = UIScreen.main.bounds.width
        menuViewWidth = (screenWidth / 2) + 72
        viewBgTop.frame.size.width = menuViewWidth + 10
        viewTop.frame.size.width = viewBgTop.frame.size.width
        
        viewButton.frame.size.width = menuViewWidth
        btnLogout.frame.size.width = viewButton.frame.size.width - 10
        btnLogout.frame.origin.x = 8
        
        imgProfile.layer.cornerRadius =   imgProfile.frame.size.height/2
        imgProfile.clipsToBounds = true
        imgProfile.layer.borderColor = appColor.appThemeGreenColor.cgColor
        imgProfile.layer.borderWidth = 3.0
        
        lblUserName.textAlignment = .center
        lblMobileNumber.textAlignment = .center
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationIdentifier), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MenuVendor.methodRemoteNotification), name: NSNotification.Name(rawValue: notificationIdentifier), object: nil)
    }
    
    @objc func methodRemoteNotification(notification:Notification){
        
        userInfoDisplay()
        
    }
    
    //MARK:- Fuctions ==========

    func menuItems() {
        arrMenu = NSMutableArray(array: [["title": "Home"],["title": "My Orders"],["title": "Cart"],["title": "Address Book"],["title": "Reward"],["title": "Notification"],["title": "Contact Us"],["title": "App Info"],["title": "Share App"]])
    }

    func userInfoDisplay(){
        
      //  activityIndicator.isHidden = false
        
       // DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.activityIndicator.isHidden = true
      //  })
        
        
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        lblUserName.text = "\(string(dictInfo, "first_name")) \(string(dictInfo, "last_name"))"
         lblMobileNumber.text = string(dictInfo, "contact_number")
   
        //product image
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dictInfo, "profile_picture")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            imgProfile.image = UIImage(named: "defalt_profile")
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
    }

    //MARK:- Button Actions
    //Mark:-SIGN OUT Action====================================================================================
    
    @IBAction func actionSignOut(_ sender: Any){
        /*
        let attributedString = NSAttributedString(string: "Confirmation", attributes: [
            NSFontAttributeName : UIFont.systemFont(ofSize: 15), //your font here
            NSForegroundColorAttributeName : appColor.blueColor//UIColor.blue
            ])
        let alert = UIAlertController(title: "", message: "Are you sure, you Want to logout?\n",  preferredStyle: .alert)
        
        alert.setValue(attributedString, forKey: "attributedTitle")
        */
        
        let alert = UIAlertController (title: "Confirmation", message: "Are you sure, you Want to logout?\n" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            self.removeAllUserDefaults()
            
            var boolPoped = false
            
            for vc in (self.navigationController?.viewControllers)! {
                
                if vc is SignInVC {
                    boolPoped = true
                  //  self.removeAllUserDefaults()
                     self.navigationController?.popToViewController(vc, animated: false)
                }
            }
            
            print("boolPoped>>\(boolPoped)")
            if !boolPoped {
                
              // self.navigationController?.viewControllers = []
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
  
            self.navigationController?.pushViewController(vc, animated: false)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 
    //SIGN OUT Action====================================================================================
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func removeAllUserDefaults()  {
         let defaults = UserDefaults.standard
        
        if let rem = defaults.object(forKey: PredefinedConstants().defRememberMe) {
            let newR = rem as! String
            
            if newR == "1"{
            }else{
              self.removeAll()
            }
        }else{
           self.removeAll()
        }
    }
    
    func removeAll(){
        PredefinedConstants.appDelegate.arrDataVegetables.removeObject(forKey: "arrDataVegetables")
        PredefinedConstants.appDelegate.arrDataFruits.removeObject(forKey: "arrDataFruits")
        PredefinedConstants.appDelegate.arrDataVegetables.synchronize()
        PredefinedConstants.appDelegate.arrDataFruits.synchronize()
        PredefinedConstants.appDelegate.defaltAutoLogin.removeObject(forKey: "defaltAutoLogin")
        PredefinedConstants.appDelegate.defaltAutoLogin.removeObject(forKey: "Password")
        PredefinedConstants.appDelegate.defaltAutoLogin.synchronize()
    }
    
    //PRofile Action
    @IBAction func btnEditProfile(_ sender: Any) {
    self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavProfile"), animated: true)
        self.sideMenuViewController.hideViewController()
    }

    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCellVendor") as! MenuCellVendor
        
        let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
        let title = string(dict, "title")
        cell.lblMenu.text = string(dict, "title")
        
        if title == "Home" {
            cell.imgMenu.image = UIImage(named: "menu_home")
            
        } else if title == "My Orders" {
            cell.imgMenu.image = UIImage(named: "menu_my_orders")
            
        } else if title == "Cart" {
            cell.imgMenu.image = UIImage(named: "menu_cart")
            
        } else if title == "Address Book" {
            cell.imgMenu.image = UIImage(named: "menu_address_book")
            
        } else if title == "Reward" {
            cell.imgMenu.image = UIImage(named: "menu_reward")
            
        } else if title == "Notification" {
            cell.imgMenu.image = UIImage(named: "menu_notification")
            
        } else if title == "Contact Us" {
            cell.imgMenu.image = UIImage(named: "menu_contact")
            
        }else if title == "App Info" {
            cell.imgMenu.image = UIImage(named: "menu_app_info")
            
        }else if title == "Share App" {
            cell.imgMenu.image = UIImage(named: "menu_share")
        }
        
        
        if indexPath.row == selectindex {
            cell.backgroundColor = UIColor(named: "appTheme")
            cell.lblMenu.textColor = UIColor.white
            
        } else {
            cell.lblMenu.textColor = UIColor.black
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblView{
            let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
            let title = string(dict, "title")
            
            arrMenu = NSMutableArray(array: [["title": "Home"],["title": "My Orders"],["title": "Cart"],["title": "Address Book"],["title": "Reward"],["title": "Notification"],["title": "Contact Us"],["title": "App Info"],["title": "Share App"]])
            
            PredefinedConstants.appDelegate.menuClass = ""
            
            if title == "Home" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavHome"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "My Orders" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavMyOrders"), animated: true)
                
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Cart" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                //Http.alert("", "Under Working")
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavCart"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Address Book" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavAddress"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Reward" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavReward"), animated: true)
                // kAppDelegate.menuPush = "menubtn"
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Notification" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavNotification"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Contact Us" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavContactUs"), animated: true)
                self.sideMenuViewController.hideViewController()
            }else if title == "App Info" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavAboutUs"), animated: true)
                self.sideMenuViewController.hideViewController()
            }else if title == "Share App" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                print("share App")
                
                let text = "Download our new app to place online order of Vegetables and fruits\nVegiDelivery: www.vegidelivery.com "
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
            }
        }else{
            
        }
        
    }
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
    
 //   let windowScreen = UIApplication.shared.keyWindow!
    /*
    override func viewWillAppear(_ animated: Bool) {
        self.userInfoDisplay()
    }
*/
    //MARK:-Notification work ===========================Neeleshwari****************************************
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
    //=================================
    let windowScreen = UIApplication.shared.keyWindow!
    
    override func viewWillAppear(_ animated: Bool) {
        btnCancel.backgroundColor = UIColor.white
        btnOK.backgroundColor = UIColor.white
        
         self.userInfoDisplay()
        
        //  let userName = string(kAppDelegate.dictUserInfo, "username")
        //   lblUserId.text! = userName
        /////====notification ============neeleshwari
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(methodRemoteNotificationMsg), name: NSNotification.Name(rawValue: "NeeleshwariGetNewNotification"), object: nil)
        
        //  self.wsTestNotification()
    }
    
    @objc func methodRemoteNotificationMsg(notification:Notification){
        // What message comes here ?
        
        print("remoteMessage.appData : ", notification.userInfo)
        
        var userInfo = notification.userInfo! as! [String:AnyObject]
        print("Neeleshwari received NOTIFICATION check actual Data ------------- \(userInfo)")
        
        //         kAppDelegate.dicc = (userInfo as? NSDictionary)!
        //
        //         dd = kAppDelegate.dictCatchNotifications
        //          print(dd)
        //
        
        if let dictNotifications : NSDictionary = (userInfo as? NSDictionary)!{
           // print("dictNotifications ----------->>>\(dictNotifications)")
            
            kAppDelegate.dictCatchNotifications = (dictNotifications.mutableCopy() as! NSDictionary) as! NSMutableDictionary
        }
        
       // print("dictCatchNotifications ----------->>>\(kAppDelegate.dictCatchNotifications)")
        lblTitel.text = string(kAppDelegate.dictCatchNotifications, "title")
        lblDescription.text = string(kAppDelegate.dictCatchNotifications, "message")
        
        addPopupShowVerification(viewMain: viewMain, viewPopUP: viewPopUP)
    }
    
    //=================================
    //MARK:- Notification Pop up//=================================
    @IBOutlet var viewMain: UIView!
    
    @IBOutlet var viewPopUP: View!
    @IBOutlet var lblTitel: UILabel!
    
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnOK: UIButton!
    
    @IBAction func btnOKAction(_ sender: Any) {
          removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
 
       // print("dictCatchNotifications ----------->>>\(kAppDelegate.dictCatchNotifications)")
        
       // print("dictCatchNotifications type ----------->>>\(string(kAppDelegate.dictCatchNotifications, "type"))")
        
        if string(kAppDelegate.dictCatchNotifications, "type") == "ORDER_DELIVERED"{
            
            var order_id = ""
            order_id =  string(kAppDelegate.dictCatchNotifications, "order_id")
            
            if order_id.count == 0{
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavNotification"), animated: true)
                self.sideMenuViewController.hideViewController()
            }else{
                PredefinedConstants.appDelegate.menuClass = "menu"
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavOrderDetail"), animated: true)
                self.sideMenuViewController.hideViewController()
            }
        }else{
            self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavNotification"), animated: true)
            self.sideMenuViewController.hideViewController()
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
          removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
        PredefinedConstants.appDelegate.menuClass = ""
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowVerification(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
         self.view.window?.addSubview(viewMain)
       // self.scroll.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
  
        viewPopUP.frame = CGRect(x: 15, y: self.view.frame.size.height/2 - viewPopUP.frame.size.height/2 , width: viewPopUP.frame.size.width, height: viewPopUP.frame.size.height)
        
       // viewPopUP.backgroundColor = UIColor.yellow
        viewPopUP.center = self.view.center
         view.frame = CGRect(x: 0, y: 0 , width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })

    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewMain.bounds.contains(touch.location(in: viewPopUP)) {
            return false
        }
        
        return true
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo// Print message ID.
       //print("Message userInfo willPresent:---> \(userInfo)")// completionHandler([])
        
        var dict = NSDictionary()
        dict = userInfo as NSDictionary
       // print("userInfodict-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-==\(dict)")
        
        let user_type = string(dict, "user_type")
        print("user_type-\(user_type)-")
        
        let defaults = UserDefaults.standard
        let str: String? = defaults.value(forKey: defaultUser) as? String
        completionHandler([.alert, .badge, .sound])
    }
   

} //class ends here....
















