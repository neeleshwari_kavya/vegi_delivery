//
//  TermsAndConditionsVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 11/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//


import Foundation
import UIKit

class TermsAndConditionsVC: UIViewController , UIWebViewDelegate{
    
    @IBOutlet var webView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = false
        setNavTransparent(className: "Terms And Conditions".uppercased())
        BackNavigationButton()
        wsTermAndConditions()
        /*
         let url = NSURL (string: "https://www.google.co.in")
         let requestObj = URLRequest(url: url! as URL)
         webView.loadRequest(requestObj)
         */
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "back-arrow"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        
        self.webView.delegate = self
        self.webView.isOpaque = false
        self.webView.backgroundColor = UIColor.clear
    }
    
    //MARK:- Webview delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
    }
    
    //MARK: WS TermAndConditions
    func wsTermAndConditions() {
        
        let params = NSMutableDictionary()
        
        Http.instance().json(WebServices().WS_TermsNConditions, params, "GET", ai: false, popup: true, prnt: true) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray{
                        
                        if result.count != 0{
                            let dictData = result.object(at: 0)
                            if let description = (dictData as AnyObject).object(forKey: "text"){
                                print("description>>\(description)")
                                self.webView.loadHTMLString(description as! String, baseURL: nil)
                            }
                        }
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//Class Ends Here ===========================.....Neeleshwari
/////////===========+Extension.........Neeleshwari






