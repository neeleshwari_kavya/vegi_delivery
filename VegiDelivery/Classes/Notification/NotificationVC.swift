//
//  NotificationVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 16/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class NotificationVC: UIViewController , UITableViewDelegate , UITableViewDataSource , NotificationCellDelegate {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblNoRecords: UILabel!
    
    //MARK:- Variables
    var arrNotificationList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    var intCount = 0
   // var lastClass = "menu"
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "NOTIFICATION")
        self.menuNavigationButton()
        self.addDeleteButton()
        self.lblNoRecords.isHidden = true
        page = 1
        self.WS_CustomerNotificationList()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    //ic_dele
    
    func addDeleteButton() {
        let buttonAdd = UIButton.init(type: .custom)
        buttonAdd.setImage(UIImage.init(named: "ic_dele_white"), for: UIControlState.normal)
        buttonAdd.addTarget(self, action:#selector(btnDeleteNotificationAction), for:.touchUpInside)
        buttonAdd.frame = CGRect.init(x: 0, y: 0, width: 30, height: 40)
        let barButton = UIBarButtonItem.init(customView: buttonAdd)
        buttonAdd.backgroundColor = UIColor.clear
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func btnDeleteNotificationAction() {
        print("btnDeleteNotificationAction")
        let arrJson = NSMutableArray()

        if intCount == 0 {
            Http.alert("", "Please select Notifications to delete.")
            
        }else{
            
            for i in 0..<arrNotificationList.count {
                let dict = (arrNotificationList.object(at: i) as! NSDictionary).mutableCopy()  as! NSMutableDictionary
                let checkedValue = string(dict, "check")
               let notification_id = string(dict, "notification_id")
                
                if checkedValue == "1"{
                    arrJson.add("\(notification_id)")
                }
            }
            self.WS_Delete_Notification(arrJson:arrJson)
        }
    }
    
    //MARK:- Fuctions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrNotificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        let dictData = arrNotificationList.object(at: indexPath.row) as! NSDictionary
        
        cell.lblTitle.text = string(dictData, "title")
        cell.lblDate.text = string(dictData, "created_at")
        cell.lblDetails.text = string(dictData, "message")
        let imgURL = string(dictData, "filename")
        cell.imgStatus.image = UIImage(url: URL(string: imgURL))
        
        let checkedValue = string(dictData, "check")
        
        if checkedValue == "1"{
            cell.imgDeleteNotification.isHidden = false
        }else{
            cell.imgDeleteNotification.isHidden = true
        }
        
        ///================
        cell.delegateNotificationCell = self
        cell.intIndex = indexPath.row
        ///================
        cell.activityIndicator.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ){
            cell.activityIndicator.isHidden = true
        }
        ///================
        cell.imgNotification.layer.cornerRadius =   cell.imgNotification.frame.size.height/2
        cell.imgNotification.clipsToBounds = true
        cell.imgNotification.layer.borderColor = UIColor.red.cgColor
        cell.imgNotification.layer.borderWidth = 0
        /*
        cell.imgDeleteNotification.layer.cornerRadius =   cell.imgDeleteNotification.frame.size.height/2
        cell.imgDeleteNotification.clipsToBounds = true
        cell.imgDeleteNotification.layer.borderColor = appColor.redColor.cgColor
        cell.imgDeleteNotification.layer.borderWidth = 2.0
        */
        if indexPath.row == (arrNotificationList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrNotificationList.count % 50 == 0 {
                    page += 1
                    self.WS_CustomerNotificationList()
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  arrNotificationList.remove(indexPath.row)
    }
    
    //MARK:- ------------------ ws_MyOrder List ----------------------//
    func WS_CustomerNotificationList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        Http.instance().json(WebServices().WS_CustomerNotificationList, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrNotificationList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                       // print("arr>>>\(arr)")
                        if self.boolWsPage {
                            self.arrNotificationList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrNotificationList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        
                        if self.arrNotificationList.count == 0 {
                            self.lblNoRecords.isHidden  = false
                            
                        }else{
                            self.lblNoRecords.isHidden  = true
                        }
                        self.setDefaultDict()
                        self.tblView.reloadData()
                    }
                    
                } else {
                    //  Http.alert("", string(json! , "message"))
                    self.lblNoRecords.isHidden  = false
                    self.lblNoRecords.text = string(json! , "message")
                    self.arrNotificationList = NSMutableArray()
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    //_WS_Delete_Notification
   // func WS_Delete_Notification(notification_id:String) {
     func WS_Delete_Notification(arrJson:NSMutableArray) {
        print("arrJson.convertToString()>>\(arrJson.convertToString())")
        
        let params = NSMutableDictionary()
        params["notification_id"] = arrJson.convertToString()
        
        Http.instance().json(WebServices().WS_Delete_Notification, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.intCount = 0
                    self.WS_CustomerNotificationList()
                   
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    func setDefaultDict() {
        
        for i in 0..<arrNotificationList.count {
            let dict = (arrNotificationList.object(at: i) as! NSDictionary).mutableCopy()  as! NSMutableDictionary
            
            dict.setValue("0", forKey: "check")
            arrNotificationList.replaceObject(at: i, with: dict)
        }
    }
    
    func btnDelete(intIndex: Int , indexPathValue:IndexPath){
     // Http.startActivityIndicator()
        
        let dict = (arrNotificationList.object(at: intIndex) as! NSDictionary).mutableCopy()  as! NSMutableDictionary
        let check = string(dict, "check")
        
        if check == "1" {
             dict.setValue("0", forKey: "check")
            if intCount != 0{
               intCount -= 1
            }
        }else{
             dict.setValue("1", forKey: "check")
             intCount += 1
        }
        arrNotificationList.replaceObject(at: intIndex, with: dict)
        
        tblView.beginUpdates()
        tblView.reloadRows(at: [IndexPath(row: intIndex, section: 0)], with: .automatic)
        tblView.endUpdates()
        
        //tblView.reloadData()
      //  Http.stopActivityIndicator()
    }
        
}//Class Ends Here ===========================.....Neeleshwari

//MARK:- NotificationCell

public protocol NotificationCellDelegate{
    
    func btnDelete(intIndex: Int, indexPathValue:IndexPath)
}
class NotificationCell :UITableViewCell{
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var imgNotification: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imgDeleteNotification: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    
    var delegateNotificationCell:NotificationCellDelegate?
    var intIndex = Int()
    var indexPathValue = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        delegateNotificationCell?.btnDelete(intIndex: intIndex, indexPathValue: indexPathValue)
    }
}


extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}





