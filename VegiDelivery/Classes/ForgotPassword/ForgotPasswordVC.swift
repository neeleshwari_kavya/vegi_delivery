//
//  ForgotPasswordVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 11/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//


import UIKit

class ForgotPasswordVC: UIViewController, UITextFieldDelegate , UIGestureRecognizerDelegate{
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var btnSubmitEmail: UIButton!
    // @IBOutlet var lblMailLine: UILabel!
    
    //Reset Password
    @IBOutlet var viewVerification: UIView!
    @IBOutlet var viewPopUpVerification: UIView!
    @IBOutlet var tfNewPassword: SkyFloatingLabelTextField!
    @IBOutlet var tfConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet var tfVerificationCode: SkyFloatingLabelTextField!
    @IBOutlet var btnCancelPassword: UIButton!
    @IBOutlet var btnSubmitPassword: UIButton!
    
    //MARK:- VARIABLES
    
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotifications()
        self.delegatesAndDisplayView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scroll.contentInset = UIEdgeInsets.zero
        self.BackNavigationButton()
        setNavTransparent(className: "FORGOT PASSWORD")
        self.hideKeyboard()
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "back-arrow"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
        self.deregisterFromKeyboardNotifications()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func buttonAction(sender: UIButton!) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Actions.
    
    @IBAction func btnSubmitEmailAction(_ sender: Any) {
        
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.WS_Forgot_Password()
            
        }
        
        //    self.addPopupShowVerification(viewMain: viewVerification, viewPopUP: viewPopUpVerification)
    }
    
    //MARK: Verification Pop UP Actions.
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        removeSubViewWithAnimation(viewMain: viewVerification, viewPopUP: viewPopUpVerification)
    }
    
    @IBAction func btnSubmitPasswordAction(_ sender: Any) {
        
        if let str = checkPasswordValidation() {
            Http.alert("", str)
        } else {
            self.WS_Reset_Password()
        }
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        tfEmail.delegate = self
        tfEmail.tintColor = UIColor.black
        
        btnSubmitEmail.border(UIColor.clear, 22, 0)
        
        let userIcon = UIImage(named: "email_id")
        setPaddingWithLeftImage(image: userIcon!, textField: tfEmail)
        
        tfEmail.placeholder = "\t\tEmail or Mobile number"
        tfEmail.title = "Email or Mobile number"
        
        tfNewPassword.textAlignment = .center
        tfConfirmPassword.textAlignment = .center
        tfVerificationCode.textAlignment = .center
        
        tfNewPassword.titleLabel.textAlignment = .center
        tfConfirmPassword.titleLabel.textAlignment = .center
        tfVerificationCode.titleLabel.textAlignment = .center
        
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self
        tfVerificationCode.delegate = self
        
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- FUNCTION FOR POPVIEW ----------------------------------------
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowVerification(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // self.view.window?.addSubview(viewMain)
        self.scroll.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
        
        tfNewPassword.text = ""
        tfConfirmPassword.text = ""
        tfVerificationCode.text = ""
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewVerification, viewPopUP: viewPopUpVerification)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewVerification.bounds.contains(touch.location(in: viewPopUpVerification)) {
            return false
        }
        
        return true
    }
    
    ///addDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        //  let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        // items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfVerificationCode.inputAccessoryView = doneToolbar
    }
    /*
     @objc func nextOfDoneTool() {
     tfPassword.becomeFirstResponder()
     }
     */
    
    func done() {
        tfVerificationCode.resignFirstResponder()
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    
    /////////__________***********************_________
    
    func checkValidation() -> String? {
        
        if tfEmail.text?.count == 0 {
            return AlertMSG.blankEmailOrMobile
            
        }else if (tfEmail.text?.count)! > 0 {
            let strResult = isNumberOrEmail(testString: tfEmail.text!)
            
            if strResult == "email" {
                if !SourabhisValidEmail(testStr:tfEmail.text!){
                    
                    return AlertMSG.invalidEmail
                }
            } else {
                let strLength = tfEmail.text?.count
                if strLength != 10 {
                    return AlertMSG.invalidMobile
                }
            }
        }
        
        return nil
    }
    func checkPasswordValidation() -> String? {
        
        if tfNewPassword.text?.count == 0 {
            return AlertMSG.blankNewPassword
        } else if (tfNewPassword.text?.count)! < 6  || (tfConfirmPassword.text?.count)! < 6  {
            return AlertMSG.passwordLength
        } else if tfConfirmPassword.text?.count == 0 {
            return AlertMSG.blankConfirmPassword
        } else if tfNewPassword.text != tfConfirmPassword.text {
            return AlertMSG.pwdMissMatch
        } else if tfVerificationCode.text?.count == 0 {
            return AlertMSG.blankVerificationCode
        } else if (tfVerificationCode.text?.count)! < 6  {
            return AlertMSG.invalidVerificationCode
        }
        
        return nil
    }
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {

            tfEmail.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        //Verifications
        if tfNewPassword == textField {
            tfConfirmPassword.becomeFirstResponder()
        } else if textField == tfConfirmPassword {
            tfVerificationCode.becomeFirstResponder()
        }else if textField == tfVerificationCode {
            tfVerificationCode.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfEmail {
            
            if (tfEmail.text?.count)! > 8 {
                if isNumberOrEmail(testString: tfEmail.text!) == "email" {
                    return (length > 40) ? false : true
                } else {
                    return (length > 10) ? false : true
                }
            }
            return (length > 20) ? false : true
            
        }
        
        if textField == tfNewPassword {
            
            return (length > 20) ? false : true
        }else if textField == tfConfirmPassword {
            
            return (length > 20) ? false : true
        }else if textField == tfVerificationCode {
            
            return (length > 6) ? false : true
        }
        return true
    }
    
    //MARK:- WS_Forgot_Password
    func WS_Forgot_Password(){
        
        view.endEditing(true)
        let params = NSMutableDictionary()
        
        if isNumberOrEmail(testString: tfEmail.text!) == "email" {
            params["email"] = tfEmail.text!
        } else {
            params["contact_number"] = tfEmail.text!
        }
        
        Http.instance().json(WebServices().WS_Forgot_Password, params, "POST", ai: true, popup: true, prnt: true, nil, nil) { (json, params) in
            let json = json as? NSDictionary
            if (json != nil) {
                print(json!)
                
                if number(json!, "success") == 1{
                    
                    self.addPopupShowVerification(viewMain: self.viewVerification, viewPopUP: self.viewPopUpVerification)
                    
                }
                
            }else{
                Http.alert(string(json!, "message"), "")
            }
        }
    }
    
    //MARK:- WS_Reset_Password
    func WS_Reset_Password(){
        
        view.endEditing(true)
        let params = NSMutableDictionary()
        params["new_password"] = tfNewPassword.text!
        params["verify_code"] = tfVerificationCode.text!
        
        print(isNumberOrEmail(testString: tfEmail.text!))
        
        if isNumberOrEmail(testString: tfEmail.text!) == "email" {
            params["email"] = tfEmail.text!
        } else {
            params["contact_number"] = tfEmail.text!
        }
        
        print(WebServices().WS_Reset_Password)
        print(params)
        
        Http.instance().json(WebServices().WS_Reset_Password, params, "POST", ai: true, popup: true, prnt: true, nil, nil) { (json, params) in
            let json = json as? NSDictionary
            if (json != nil) {
                print(json!)
                
                if number(json!, "success") == 1{
                    self.removeSubViewWithAnimation(viewMain: self.viewVerification, viewPopUP:self.viewPopUpVerification)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            }else{
                Http.alert(string(json!, "message"), "")
            }
        }
    }
}//Class Ends Here ===========================.....Neeleshwari






