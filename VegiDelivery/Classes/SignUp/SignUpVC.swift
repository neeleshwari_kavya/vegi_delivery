//
//  SignUpVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 10/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class SignUpVC: UIViewController , UIGestureRecognizerDelegate , UITextFieldDelegate{
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfMobile: SkyFloatingLabelTextField!
    @IBOutlet var tfPassword: SkyFloatingLabelTextField!
    @IBOutlet var btnTermsConditions: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnTermsChecked: UIButton!
    
    //Mark:- Variables
    var isChecked = Bool()
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.automaticallyAdjustsScrollViewInsets = false
        self.addDoneButtonOnKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scrlView.contentInset = UIEdgeInsets.zero
        self.BackNavigationButton()
        setNavTransparent(className: "SIGN UP")
        self.registerForKeyboardNotifications()
        btnSignUp.isUserInteractionEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.hideKeyboard()
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "back-arrow"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- FUNCTIONS
    func displayAndDelegates()  {
        tfEmail.delegate = self
        tfMobile.delegate = self
        tfPassword.delegate = self
        
        let userIcon = UIImage(named: "email_id")
        setPaddingWithRightImage(image: userIcon!, textField: tfEmail)
        
        let userIconMobile = UIImage(named: "mobile")
        setPaddingWithRightImage(image: userIconMobile!, textField: tfMobile)
        
        let userIconPassword = UIImage(named: "password")
        setPaddingWithRightImage(image: userIconPassword!, textField: tfPassword)
        btnSignUp.border(UIColor.clear, 22, 0)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        let imgUncheck = UIImage(named: "uncheck")
        btnTermsChecked.isSelected = true
        btnTermsChecked.setImage(imgUncheck, for: .normal)
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
             btnSignUp.isUserInteractionEnabled = false
             self.WS_SignUp()
        }
        
    }
    
    @IBAction func btnTermsConditionsAction(_ sender: Any) {
        print("btnTermsConditionsAction")
        
        let newVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func btnTermsCheckedAction(_ sender: Any) {
        
        self.hideKeyboard()
        print("btnTermsCheckedAction")
        let imgCheck = UIImage(named: "checked")
        let imgUncheck = UIImage(named: "uncheck")
        
        if btnTermsChecked.isSelected{
            btnTermsChecked.isSelected = false
            btnTermsChecked.setImage(imgCheck, for: .normal)
            isChecked = true
        }else{
            btnTermsChecked.isSelected = true
            btnTermsChecked.setImage(imgUncheck, for: .normal)
            isChecked = false
        }
    }
    
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if tfEmail.text?.count == 0  {
            return AlertMSG.blankEmail
            
        }else if !SourabhisValidEmail(testStr:tfEmail.text!){
            return AlertMSG.invalidEmail
        }else if  tfMobile.text?.count == 0  {
            return AlertMSG.blankMobile
            
        } else if (tfMobile.text?.count)! < 10 {
            return AlertMSG.invalidMobile
            
        } else if (tfPassword.text?.count)! == 0 {
            return AlertMSG.blankPassword
            
        }else if (tfPassword.text?.count)! < 6 {
            return AlertMSG.passwordLength
        }else if !isChecked{
            
            return AlertMSG.acceptConditions
        }
        return nil
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if tfEmail == textField {
            tfMobile.becomeFirstResponder()
        } else if textField == tfMobile {
            tfPassword.becomeFirstResponder()
        }else if textField == tfPassword {
            tfPassword.resignFirstResponder()
            scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfEmail {
            return (length > 40) ? false : true
        }else if textField == tfPassword {
            return (length > 10) ? false : true
        }
        else if textField == tfMobile {
            return (length > 10) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSignUp.frame.origin.y + btnSignUp.frame.size.height + 10)
    }
    
    ///addDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfMobile.inputAccessoryView = doneToolbar
    }
    
    @objc func nextOfDoneTool() {
        tfPassword.becomeFirstResponder()
    }
    
    func done() {
        tfEmail.resignFirstResponder()
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    
    
    func WS_SignUp(){
        
        var name = ""
        
        if DeviceType.IS_IPHONE_5 {
            name = "iPhone 5"
        }else if DeviceType.IS_IPHONE_6 {
            name = "iPhone 6"
        }else if DeviceType.IS_IPHONE_6P {
            name = "iPhone 6 Plus"
        }else if DeviceType.IS_IPHONE_7 {
            name = "iPhone 7"
        }else if DeviceType.IS_IPHONE_7P {
            name = "iPhone 7 Plus"
        }else{
            name = "iPhone X"
        }
        
        print("name---->>>>\(name)")
        //
        let params = NSMutableDictionary(dictionary:[
            "contact_number": tfMobile.text!,
            "email": tfEmail.text!,
            "password": tfPassword.text!,
            "device_id":  PredefinedConstants.userDeviceId,
            "device_type": "iOS",
            "device_name":name,
            "notification_id": PredefinedConstants.appDelegate.strNotificationToken,
            "app_version": PredefinedConstants.appVersion!,
            "referrer_code": "123456"
            ])
        
        print("WebServices().WS_SignUp....>>>\(WebServices().WS_SignUp)")
        Http.instance().json(WebServices().WS_SignUp, params, "POST", ai: true, popup: true, prnt: true, nil) { (json, params) in
            let json = json as? NSDictionary
            if (json != nil) {
                if number(json!, "success") == 1 {
                    if let result = (json as AnyObject).object(forKey: "result") as? NSDictionary{
                        
                        print("Sign up success")

                        PredefinedConstants.appDelegate.dictUserInformations = result.mutableCopy() as! NSMutableDictionary
                        print("dictUserInformations--->>>> ...\( PredefinedConstants.appDelegate.dictUserInformations)")
                        
                        //PredefinedConstants.appDelegate.defalt_RemainingSignUp.synchronize()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC//SignupSuccess
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
            
                } else {
                    Http.alert(string(json!, "message"), "")
                }
            }else {
                Http.alert(string(json!, "message"), "")
            }
        }
    }

}//Class Ends Here ===========================.....Neeleshwari











